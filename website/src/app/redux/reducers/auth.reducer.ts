import { ReduxState, ActionTypes, Action } from '../../interfaces/redux-state';
import * as authActions from '../actions/auth.actions';
const initialState: ReduxState = {  user: null };


export function UserReducer(state = initialState, action: Action): ReduxState {

    const payload = action.payload;

    switch (action.type) {
        case ActionTypes.FILL_USER:
            console.log("REDUX");
            console.log(state);
            console.log("//////////////");
            console.log(action);
            return {
                ...state.user, ...payload
            };
        case ActionTypes.UPDATE_USER:
            return {
                ...state.user, ...payload
            };
    
        default:
            return state;
    }

}