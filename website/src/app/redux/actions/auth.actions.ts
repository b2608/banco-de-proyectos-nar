import { Action } from '@ngrx/store';
import { User } from '../../interfaces/user';
import { ActionTypes } from '../../interfaces/redux-state';

export class FillUserData implements Action {
    type = ActionTypes.FILL_USER;
   
    constructor(public payload: User) { }
   }

   export class UpdateUserData implements Action {
    type = ActionTypes.UPDATE_USER;
   
    constructor(public payload: User) { }
   }

export type UserActions
 = FillUserData |
 UpdateUserData