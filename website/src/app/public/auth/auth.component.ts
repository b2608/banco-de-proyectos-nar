import { Component, OnInit, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { 
  UserLogin, 
  UserRegister, 
  OrganizationCityTypeMapper, 
  OrganizationCoverageMapper, 
  OrganizationTypeMapper,
  LinesMapper,
  ComponentsMapper } from '@app/interfaces/user';
import { ApiResponse } from '@app/interfaces/api';
import { ReduxState } from '@app/interfaces/redux-state';
import { ActionTypes } from '@app/interfaces/redux-state';
import { AuthService } from '@app/services/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit {
  public modalRef: BsModalRef | undefined;
  active = 1;
  user$!: Observable<UserRegister>;
  newUser!: UserRegister;
  registerform!: FormGroup;
  existingUser!: UserLogin;
  loginform!: FormGroup;
  submittedForm: boolean = false;
  sending: boolean = false;
  orgTypesMapper = OrganizationTypeMapper;
  coverageMapper = OrganizationCoverageMapper;
  organizationCityTypeMapper = OrganizationCityTypeMapper;
  componentsMapper = ComponentsMapper;
  linesMapper = LinesMapper;

  constructor(
    private store: Store<ReduxState>, 
    private formBuilder: FormBuilder, 
    private router: Router, 
    private authService: AuthService,
    private modalService: BsModalService
    ) { 
    this.store.subscribe(state => {
      console.log(state);
      this.user$ = store.select('user');
      this.user$.subscribe(data => this.newUser = data);
    })

  }

  ngOnInit(): void {
    this.registerform = this.formBuilder.group({
      organizationName: ["", Validators.required],
      organizationDNI: ["", Validators.required],
      legalRepresentative: ["", Validators.required],
      legalRepresentativeDNI: ["", Validators.required],
      organizationType: [null, Validators.required],
      coverage: [null, Validators.required],
      comunity: [null, Validators.required],
      state: ["", Validators.required],
      city: ["", Validators.required],
      township: [""],
      phoneNumber: [0, Validators.required],
      email: ["", Validators.required],
      website: [""],
      password: ["", Validators.required],
      publicRegister: this.formBuilder.group({
        registerNumber: ["", Validators.required],
        year: [0, Validators.required],
        accreditingEntity: ["", Validators.required],
        entityThatAccreditsTheUpdate: ["", Validators.required],
        yearOfAccreditation: [0, Validators.required],
      }),
      /* projectPresentation: this.formBuilder.group({
        projectName: ["", Validators.required],
        publicRegister: this.formBuilder.group({
          registerNumber: ["", Validators.required],
          year: [0, Validators.required],
          accreditingEntity: ["", Validators.required],
          entityThatAccreditsTheUpdate: ["", Validators.required],
          yearOfAccreditation: [0, Validators.required],
        }),
        component: this.formBuilder.group({
          component: [null, Validators.required],
          state: [""],
        }),
        line: [null, Validators.required],
      }), */
      saveLogin: [false],
    });
    this.loginform = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
    })
    
  }

  async register(template: TemplateRef<any>){
    this.registerform.disable();
    this.sending = true;
    this.authService.register(this.registerform.value).subscribe((data: ApiResponse) => {
      if(data.code == 500){
        alert("Al parecer tenemos un error con el servidor, intentalo mas tarde");
      }
      if(data.code == 404){
        alert("Este usuario ya existe, intentelo con otro correo electronico");
      }
      if(data.code == 100){
        this.newUser = this.registerform.value;
        this.registerform.reset();
        this.openModal(template);
      }
      this.sending = false;
      this.registerform.enable();
    });
  //  await this.store.dispatch({type: ActionTypes.FILL_USER, payload: this.user});
    //await this.store.dispatch({type: ActionTypes.FILL_USER, payload: this.user});
    //console.log(this.store);
  //  this.router.navigate(['/user'])

  }

  async login(template: TemplateRef<any>){
    this.sending = true;
    this.loginform.disable();
    this.authService.login(this.loginform.value).subscribe((data: ApiResponse) => {
      if(data.code == 500){
        alert("Al parecer tenemos un error con el servidor, intentalo mas tarde");
      }
      if(data.code == 404){
        alert("Este usuario ya existe, intentelo con otro correo electronico");
      }
      if(data.code == 100){
        this.newUser = this.loginform.value;
        this.loginform.reset();
        localStorage.setItem('authToken', data.token);
        this.router.navigate(['/user']);
      }
      this.sending = false;
      this.loginform.enable();
    });
  //  await this.store.dispatch({type: ActionTypes.FILL_USER, payload: this.user});
    //await this.store.dispatch({type: ActionTypes.FILL_USER, payload: this.user});
    //console.log(this.store);
  //  this.router.navigate(['/user'])

  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
