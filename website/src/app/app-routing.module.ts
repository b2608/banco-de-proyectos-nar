import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './public/home/home.component';
import { AuthComponent } from './public/auth/auth.component';
import { PageComponent as UserPage } from './user/page/page.component';
import { ProjectsComponent } from './user/private/projects/projects.component';
import { NewDetailComponent } from './user/private/projects/new-detail/new-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent
  },
  // {
  //   path: 'auth',
  //   component: AuthComponent
  // },
   {
     path: 'user',
     component: UserPage
  },
  {
    path: 'user/projects',
    component: ProjectsComponent
  },
  {
    path: 'user/projects/in-project',
    component: NewDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
