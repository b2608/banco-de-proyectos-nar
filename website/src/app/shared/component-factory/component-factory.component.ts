import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-factory',
  templateUrl: './component-factory.component.html',
  styleUrls: ['./component-factory.component.sass']
})
export class ComponentFactoryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
