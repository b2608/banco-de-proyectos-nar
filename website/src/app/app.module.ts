import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './public/home/home.component';
import { AuthComponent } from './public/auth/auth.component';
import { ButtonComponent } from './common/button/button.component';
import { LinkComponent } from './common/link/link.component';
import { NavbarComponent } from './templates/navbar/navbar.component';
import { BannerComponent } from './templates/banner/banner.component';
import { OffersSliderComponent } from './templates/offers-slider/offers-slider.component';
import { TitleComponent } from './common/title/title.component';
import { TextComponent } from './common/text/text.component';
import { OfferComponent } from './templates/offer/offer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UserReducer } from './redux/reducers/auth.reducer';
import { ProfileComponent } from './user/private/profile/profile.component';
import { SettingsComponent } from './user/private/settings/settings.component';
import { FollowingComponent } from './user/public/following/following.component';
import { PageComponent } from './user/page/page.component';
import { InterceptorsInterceptor } from './services/interceptors.interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProjectsComponent } from './user/private/projects/projects.component';
import { NewComponent } from './user/private/projects/new/new.component';
import { NewDetailComponent } from './user/private/projects/new-detail/new-detail.component';
import { StatePresentationComponent } from './user/private/projects/new-detail/state-presentation/state-presentation.component';
import { ProjectPresentationComponent } from './user/private/projects/new-detail/project-presentation/project-presentation.component';
import { IncomeComponent } from './user/private/projects/new-detail/income/income.component';
import { CostsAndExpensesComponent } from './user/private/projects/new-detail/costs-and-expenses/costs-and-expenses.component';
import { ComponentFactoryComponent } from './shared/component-factory/component-factory.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AuthComponent,
    ButtonComponent,
    LinkComponent,
    NavbarComponent,
    BannerComponent,
    OffersSliderComponent,
    TitleComponent,
    TextComponent,
    OfferComponent,
    ProfileComponent,
    SettingsComponent,
    FollowingComponent,
    PageComponent,
    ProjectsComponent,
    NewComponent,
    NewDetailComponent,
    StatePresentationComponent,
    ProjectPresentationComponent,
    IncomeComponent,
    CostsAndExpensesComponent,
    ComponentFactoryComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    StoreModule.forRoot({ user: UserReducer }),
    StoreDevtoolsModule.instrument({
      maxAge: 4
    }),
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule 
  ],
  exports: [],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptorsInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
