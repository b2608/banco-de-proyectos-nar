import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { FormGroup } from '@angular/forms';
import { User, OrganizationCoverageMapper, OrganizationTypeMapper } from '@app/interfaces/user';
import { ReduxState } from '@app/interfaces/redux-state';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  user$!: Observable<User>;
  user!: User;
  loginform!: FormGroup;
  submittedForm: boolean = false;
  organizationCoverage = OrganizationCoverageMapper;
  organizationTypeMApper = OrganizationTypeMapper;
  coverage!: any;
  organizationType!: any;
  loading: boolean = true;

  constructor(
    private store: Store<ReduxState>
    ) { 

    this.store.subscribe(state => {
      console.log(state);
      this.user$ = store.select('user');
      this.user$.subscribe(data =>{
        this.user = data;
        this.coverage = this.organizationCoverage.find(element => element.key == this.user.coverage);
        this.organizationType = this.organizationTypeMApper.find(element => element.key == this.user.organizationType);
        
        setTimeout(()=> {
          this.loading = false;
        }, 3000);
      });
    });
  }

  ngOnInit(): void {
    
  }
}
