export const Sections = [
    {
        id: 0,
        sectionName: "Sección A. Agricultura, Ganadería, Caza, Silvicultura y Pesca",
        divisions: [
            {
                id: 0,
                divisionName: "División 01. Agricultura, ganadería, caza y actividades de servicios conexas",
                activities: [
                    {
                      code: 11,
                      activity: "Cultivos agrícolas transitorios.",
                      
                    },
                    {
                      code: 111,
                      activity: "Cultivo de cereales (excepto arroz), legumbres y semillas oleaginosas.",
                      
                    },
                    {
                      code: 112,
                      activity: "Cultivo de arroz.",
                      
                    },
                    {
                      code: 113,
                      activity: "Cultivo de hortalizas, raíces y tubérculos.",
                      
                    },
                    {
                      code: 114,
                      activity: "Cultivo de tabaco.",
                      
                    },
                    {
                      code: 115,
                      activity: "Cultivo de plantas textiles.",
                      
                    },
                    {
                      code: 119,
                      activity: "Otros cultivos transitorios n.c.p.",
                      
                    },
                    {
                      code: 12,
                      activity: "Cultivos agrícolas permanentes.",
                      
                    },
                    {
                      code: 121,
                      activity: "Cultivo de frutas tropicales y subtropicales.",
                      
                    },
                    {
                      code: 122,
                      activity: "Cultivo de plátano y banano.",
                      
                    },
                    {
                      code: 123,
                      activity: "Cultivo de café.",
                      
                    },
                    {
                      code: 124,
                      activity: "Cultivo de caña de azúcar.",
                      
                    },
                    {
                      code: 125,
                      activity: "Cultivo de flor de corte.",
                      
                    },
                    {
                      code: 126,
                      activity: "Cultivo de palma para aceite (palma africana) y otros frutos oleaginosos.",
                      
                    },
                    {
                      code: 127,
                      activity: "Cultivo de plantas con las que se preparan bebidas.",
                      
                    },
                    {
                      code: 128,
                      activity: "Cultivo de especias y de plantas aromáticas y medicinales.",
                      
                    },
                    {
                      code: 129,
                      activity: "Otros cultivos permanentes n.c.p.",
                      
                    },
                    {
                      code: 13,
                      activity: "Propagación de plantas (actividades de los viveros, excepto viveros forestales).",
                      
                    },
                    {
                      code: 130,
                      activity: "Propagación de plantas (actividades de los viveros, excepto viveros forestales).",
                      
                    },
                    {
                      code: 14,
                      activity: "Ganadería.",
                      
                    },
                    {
                      code: 141,
                      activity: "Cría de ganado bovino y bufalino.",
                      
                    },
                    {
                      code: 142,
                      activity: "Cría de caballos y otros equinos.",
                      
                    },
                    {
                      code: 143,
                      activity: "Cría de ovejas y cabras.",
                      
                    },
                    {
                      code: 144,
                      activity: "Cría de ganado porcino.",
                      
                    },
                    {
                      code: 145,
                      activity: "Cría de aves de corral.",
                      
                    },
                    {
                      code: 149,
                      activity: "Cría de otros animales n.c.p.",
                      
                    },
                    {
                      code: 15,
                      activity: "Explotación mixta (agrícola y pecuaria).",
                      
                    },
                    {
                      code: 150,
                      activity: "Explotación mixta (agrícola y pecuaria).",
                      
                    },
                    {
                      code: 16,
                      activity: "Actividades de apoyo a la agricultura y la ganadería, y actividades posteriores a la cosecha.",
                      
                    },
                    {
                      code: 161,
                      activity: "Actividades de apoyo a la agricultura.",
                      
                    },
                    {
                      code: 162,
                      activity: "Actividades de apoyo a la ganadería.",
                      
                    },
                    {
                      code: 163,
                      activity: "Actividades posteriores a la cosecha.",
                      
                    },
                    {
                      code: 164,
                      activity: "Tratamiento de semillas para propagación.",
                      
                    },
                    {
                      code: 17,
                      activity: "Caza ordinaria y mediante trampas y actividades de servicios conexas.",
                      
                    },
                    {
                      code: 170,
                      activity: "Caza ordinaria y mediante trampas y actividades de servicios conexas.",
                      
                    }
                  ]
            },
            {
                id: 1,
                divisionName: "División 02. Silvicultura y extraccion de madera",
                activities: [
                    {
                      code: 21,
                      activity: "Silvicultura y otras actividades forestales.",
                    },
                    {
                      code: 210,
                      activity: "Silvicultura y otras actividades forestales.",
                    },
                    {
                      code: 22,
                      activity: "Extracción de madera.",
                    },
                    {
                      code: 220,
                      activity: "Extracción de madera.",
                    },
                    {
                      code: 23,
                      activity: "Recolección de productos forestales diferentes a la madera.",
                    },
                    {
                      code: 230,
                      activity: "Recolección de productos forestales diferentes a la madera.",
                    },
                    {
                      code: 24,
                      activity: "Servicios de apoyo a la silvicultura.",
                    },
                    {
                      code: 240,
                      activity: "Servicios de apoyo a la silvicultura.",
                    }
                  ]
            },
            {
                id: 2,
                divisionName: "División 03. Pesca y acuicultura",
                activities: [
                    {
                      code: 31,
                      activity: "Pesca.",
                    },
                    {
                      code: 311,
                      activity: "Pesca marítima.",
                    },
                    {
                      code: 312,
                      activity: "Pesca de agua dulce.",
                    },
                    {
                      code: 32,
                      activity: "Acuicultura.",
                    },
                    {
                      code: 321,
                      activity: "Acuicultura marítima.",
                    },
                    {
                      code: 322,
                      activity: "Acuicultura de agua dulce.",
                    }
                  ] 
            }
        ]
    },
    {
        id: 1,
        sectionName: "Sección B. Explotación de Minas y Canteras ",
        divisions: [
            {
                id: 0,
                divisionName: "División 05. Extracción de carbón de piedra y lignito",
                activities: [
                    {
                      code: 51,
                      activity: "Extracción de hulla (carbón de piedra).",
                    },
                    {
                      code: 510,
                      activity: "Extracción de hulla (carbón de piedra).",
                    },
                    {
                      code: 52,
                      activity: "Extracción de carbón lignito.",
                    },
                    {
                      code: 520,
                      activity: "Extracción de carbón lignito.",
                    }
                  ]
            },
            {
                id: 1,
                divisionName: "División 06. Extracción de petróleo crudo y gas natural",
                activities: [
                    {
                      code: 61,
                      activity: "Extracción de petróleo crudo.",
                    },
                    {
                      code: 610,
                      activity: "Extracción de petróleo crudo.",
                    },
                    {
                      code: 62,
                      activity: "Extracción de gas natural.",
                    },
                    {
                      code: 620,
                      activity: "Extracción de gas natural.",
                    }
                  ]
            },
            {
                id: 2,
                divisionName: "División 07. Extracción de minerales metalíferos",
                activities: [
                    {
                      code: 71,
                      activity: "Extracción de minerales de hierro.",
                    },
                    {
                      code: 710,
                      activity: "Extracción de minerales de hierro.",
                    },
                    {
                      code: 72,
                      activity: "Extracción de minerales metalíferos no ferrosos.",
                    },
                    {
                      code: 721,
                      activity: "Extracción de minerales de uranio y de torio.",
                    },
                    {
                      code: 722,
                      activity: "Extracción de oro y otros metales preciosos.",
                    },
                    {
                      code: 723,
                      activity: "Extracción de minerales de níquel.",
                    },
                    {
                      code: 729,
                      activity: "Extracción de otros minerales metalíferos no ferrosos n.c.p.",
                    }
                  ]
            },
            {
                id: 3,
                divisionName: "División 08.  Extracción de otras minas y canteras",
                activities: [
                    {
                      code: 81,
                      activity: "Extracción de piedra, arena, arcillas, cal, yeso, caolín, bentonitas y similares.",
                    },
                    {
                      code: 811,
                      activity: "Extracción de piedra, arena, arcillas comunes, yeso y anhidrita.",
                    },
                    {
                      code: 812,
                      activity: "Extracción de arcillas de uso industrial, caliza, caolín y bentonitas.",
                    },
                    {
                      code: 82,
                      activity: "Extracción de esmeraldas, piedras preciosas y semipreciosas.",
                    },
                    {
                      code: 820,
                      activity: "Extracción de esmeraldas, piedras preciosas y semipreciosas.",
                    },
                    {
                      code: 89,
                      activity: "Extracción de otros minerales no metálicos n.c.p.",
                    },
                    {
                      code: 891,
                      activity: "Extracción de minerales para la fabricación de abonos y productos químicos.",
                    },
                    {
                      code: 892,
                      activity: "Extracción de halita (sal).",
                    },
                    {
                      code: 899,
                      activity: "Extracción de otros minerales no metálicos n.c.p.",
                    }
                  ]
            },
            {
                id: 4,
                divisionName: "División 09. Actividades de servicios de apoyo para la explotación de minas y canteras",
                activities: [
                    {
                      code: 91,
                      activity: "Actividades de apoyo para la extracción de petróleo y de gas natural.",
                    },
                    {
                      code: 910,
                      activity: "Actividades de apoyo para la extracción de petróleo y de gas natural.",
                    },
                    {
                      code: 99,
                      activity: "Actividades de apoyo para otras actividades de explotación de minas y canteras.",
                    },
                    {
                      code: 990,
                      activity: "Actividades de apoyo para otras actividades de explotación de minas y canteras.",
                    }
                  ]
            },
        ]
    },
    {
        id: 2,
        sectionName: "Sección C. Industrias Manufactureras",
        divisions: [
            {
                id: 0,
                divisionName: "División 10. Elaboración de productos alimenticios",
                activities: [
                    {
                      code: 101,
                      activity: "Procesamiento y conservación de carne, pescado, crustáceos y moluscos."
                    },
                    {
                      code: 1011,
                      activity: "Procesamiento y conservación de carne y productos cárnicos."
                    },
                    {
                      code: 1012,
                      activity: "Procesamiento y conservación de pescados, crustáceos y moluscos."
                    },
                    {
                      code: 102,
                      activity: "Procesamiento y conservación de frutas, legumbres, hortalizas y tubérculos."
                    },
                    {
                      code: 1020,
                      activity: "Procesamiento y conservación de frutas, legumbres, hortalizas y tubérculos."
                    },
                    {
                      code: 103,
                      activity: "Elaboración de aceites y grasas de origen vegetal y animal."
                    },
                    {
                      code: 1030,
                      activity: "Elaboración de aceites y grasas de origen vegetal y animal."
                    },
                    {
                      code: 104,
                      activity: "Elaboración de productos lácteos."
                    },
                    {
                      code: 1040,
                      activity: "Elaboración de productos lácteos."
                    },
                    {
                      code: 105,
                      activity: "Elaboración de productos de molinería, almidones y productos derivados del almidón."
                    },
                    {
                      code: 1051,
                      activity: "Elaboración de productos de molinería."
                    },
                    {
                      code: 1052,
                      activity: "Elaboración de almidones y productos derivados del almidón."
                    },
                    {
                      code: 106,
                      activity: "Elaboración de productos de café."
                    },
                    {
                      code: 1061,
                      activity: "Trilla de café."
                    },
                    {
                      code: 1062,
                      activity: "Descafeinado, tostión y molienda del café."
                    },
                    {
                      code: 1063,
                      activity: "Otros derivados del café."
                    },
                    {
                      code: 107,
                      activity: "Elaboración de azúcar y panela."
                    },
                    {
                      code: 1071,
                      activity: "Elaboración y refinación de azúcar."
                    },
                    {
                      code: 1072,
                      activity: "Elaboración de panela."
                    },
                    {
                      code: 108,
                      activity: "Elaboración de otros productos alimenticios."
                    },
                    {
                      code: 1081,
                      activity: "Elaboración de productos de panadería."
                    },
                    {
                      code: 1082,
                      activity: "Elaboración de cacao, chocolate y productos de confitería."
                    },
                    {
                      code: 1083,
                      activity: "Elaboración de macarrones, fideos, alcuzcuz y productos farináceos similares."
                    },
                    {
                      code: 1084,
                      activity: "Elaboración de comidas y platos preparados."
                    },
                    {
                      code: 1089,
                      activity: "Elaboración de otros productos alimenticios n.c.p."
                    },
                    {
                      code: 109,
                      activity: "Elaboración de alimentos preparados para animales."
                    },
                    {
                      code: 1090,
                      activity: "Elaboración de alimentos preparados para animales."
                    }
                  ]
            },
            {
                id: 1,
                divisionName: "División 11. Elaboración de bebidas",
                activities: [
      {
        code: 110,
        activity: "Elaboración de bebidas.",
      },
      {
        code: 1101,
        activity: "Destilación, rectificación y mezcla de bebidas alcohólicas.",
      },
      {
        code: 1102,
        activity: "Elaboración de bebidas fermentadas no destiladas.",
      },
      {
        code: 1103,
        activity: "Producción de malta, elaboración de cervezas y otras bebidas malteadas.",
      },
      {
        code: 1104,
        activity: "Elaboración de bebidas no alcohólicas, producción de aguas minerales y de otras aguas embotelladas.",
      }]
            },
            {
                id: 2,
                divisionName: "División 12. Elaboración de productos de tabaco",
                activities: [
                    {
                      code: 120,
                      activity: "Elaboración de productos de tabaco."
                    },
                    {
                      code: 1200,
                      activity: "Elaboración de productos de tabaco."
                    },                  ]
            },
            {
                id: 3,
                divisionName: "División 13. Fabricación de productos textiles",
                activities: [
                    {
                      code: 131,
                      activity: "Preparación, hilatura, tejeduría y acabado de productos textiles."
                    },
                    {
                      code: 1311,
                      activity: "Preparación e hilatura de fibras textiles."
                    },
                    {
                      code: 1312,
                      activity: "Tejeduría de productos textiles."
                    },
                    {
                      code: 1313,
                      activity: "Acabado de productos textiles."
                    },
                    {
                      code: 139,
                      activity: "Fabricación de otros productos textiles."
                    },
                    {
                      code: 1391,
                      activity: "Fabricación de tejidos de punto y ganchillo."
                    },
                    {
                      code: 1392,
                      activity: "Confección de artículos con materiales textiles, excepto prendas de vestir."
                    },
                    {
                      code: 1393,
                      activity: "Fabricación de tapetes y alfombras para pisos."
                    },
                    {
                      code: 1394,
                      activity: "Fabricación de cuerdas, cordeles, cables, bramantes y redes."
                    },
                    {
                      code: 1399,
                      activity: "Fabricación de otros artículos textiles n.c.p."
                    },                  ]
            },
            {
                id: 4,
                divisionName: "División 14. Confección de prendas de vestir",
                activities: [
                    {
                      code: 141,
                      activity: "Confección de prendas de vestir, excepto prendas de piel."
                    },
                    {
                      code: 1410,
                      activity: "Confección de prendas de vestir, excepto prendas de piel."
                    },
                    {
                      code: 142,
                      activity: "Fabricación de artículos de piel."
                    },
                    {
                      code: 1420,
                      activity: "Fabricación de artículos de piel."
                    },
                    {
                      code: 143,
                      activity: "Fabricación de artículos de punto y ganchillo."
                    },
                    {
                      code: 1430,
                      activity: "Fabricación de artículos de punto y ganchillo."
                    },                  ]
            },
            {
                id: 5,
                divisionName: "División 15. Curtido y recurtido de cueros; fabricación de calzado; fabricación de artículos de viaje, maletas, bolsos de mano y artículos similares, y fabricación de artículos de talabartería y guarnicionería; adobo y teñido de pieles.",
                activities: [
                    {
                      code: 151,
                      activity: "Curtido y recurtido de cueros; fabricación de artículos de viaje, bolsos de mano y artículos similares, y fabricación de artículos de talabartería y guarnicionería, adobo y teñido de pieles."
                    },
                    {
                      code: 1511,
                      activity: "Curtido y recurtido de cueros; recurtido y teñido de pieles."
                    },
                    {
                      code: 1512,
                      activity: "Fabricación de artículos de viaje, bolsos de mano y artículos similares elaborados en cuero, y fabricación de artículos de talabartería y guarnicionería."
                    },
                    {
                      code: 1513,
                      activity: "Fabricación de artículos de viaje, bolsos de mano y artículos similares; artículos de talabartería y guarnicionería elaborados en otros materiales."
                    },
                    {
                      code: 152,
                      activity: "Fabricación de calzado."
                    },
                    {
                      code: 1521,
                      activity: "Fabricación de calzado de cuero y piel, con cualquier tipo de suela."
                    },
                    {
                      code: 1522,
                      activity: "Fabricación de otros tipos de calzado, excepto calzado de cuero y piel."
                    },
                    {
                      code: 1523,
                      activity: "Fabricación de partes del calzado."
                    },                  ]
            },
            {
                id: 6,
                divisionName: "División 16. Transformación de la madera y fabricación de productos de madera y de corcho, excepto muebles; fabricación de artículos de cestería y espartería",
                activities: [
                    {
                      code: 161,
                      activity: "Aserrado, acepillado e impregnación de la madera."
                    },
                    {
                      code: 1610,
                      activity: "Aserrado, acepillado e impregnación de la madera."
                    },
                    {
                      code: 162,
                      activity: "Fabricación de hojas de madera para enchapado; fabricación de tableros contrachapados, tableros laminados, tableros de partículas y otros tableros y paneles."
                    },
                    {
                      code: 1620,
                      activity: "Fabricación de hojas de madera para enchapado; fabricación de tableros contra­chapados, tableros laminados, tableros de partículas y otros tableros y paneles."
                    },
                    {
                      code: 163,
                      activity: "Fabricación de partes y piezas de madera, de carpintería y ebanistería para la construcción."
                    },
                    {
                      code: 1630,
                      activity: "Fabricación de partes y piezas de madera, de carpintería y ebanistería para la construcción."
                    },
                    {
                      code: 164,
                      activity: "Fabricación de recipientes de madera."
                    },
                    {
                      code: 1640,
                      activity: "Fabricación de recipientes de madera."
                    },
                    {
                      code: 169,
                      activity: "Fabricación de otros productos de madera; fabricación de artículos de corcho, cestería y espartería."
                    },
                    {
                      code: 1690,
                      activity: "Fabricación de otros productos de madera; fabricación de artículos de corcho, cestería y espartería."
                    },                  ]
            },
            {
                id: 7,
                divisionName: "División 17. Fabricación de papel, cartón y productos de papel y cartón",
                activities: [
                    {
                      code: 170,
                      activity: "Fabricación de papel, cartón y productos de papel y cartón."
                    },
                    {
                      code: 1701,
                      activity: "Fabricación de pulpas (pastas) celulósicas; papel y cartón."
                    },
                    {
                      code: 1702,
                      activity: "Fabricación de papel y cartón ondulado (corrugado); fabricación de envases, empaques y de embalajes de papel y cartón."
                    },
                    {
                      code: 1709,
                      activity: "Fabricación de otros artículos de papel y cartón."
                    },                  ]
            },
            {
                id: 8,
                divisionName: "División 18. Actividades de impresión y de producción de copias a partir de grabaciones originales",
                activities: [
                    {
                      code: 181,
                      activity: "Actividades de impresión y actividades de servicios relacionados con la impresión."
                    },
                    {
                      code: 1811,
                      activity: "Actividades de impresión."
                    },
                    {
                      code: 1812,
                      activity: "Actividades de servicios relacionados con la impresión."
                    },
                    {
                      code: 182,
                      activity: "Producción de copias a partir de grabaciones originales."
                    },
                    {
                      code: 1820,
                      activity: "Producción de copias a partir de grabaciones originales."
                    },                  ]
            },
            {
                id: 9,
                divisionName: "División 19. Coquización, fabricación de productos de la refinación del petróleo y actividad de mezcla de combustibles",
                activities: [
                    {
                      code: 191,
                      activity: "Fabricación de productos de hornos de coque."
                    },
                    {
                      code: 1910,
                      activity: "Fabricación de productos de hornos de coque."
                    },
                    {
                      code: 192,
                      activity: "Fabricación de productos de la refinación del petróleo."
                    },
                    {
                      code: 1921,
                      activity: "Fabricación de productos de la refinación del petróleo."
                    },
                    {
                      code: 1922,
                      activity: "Actividad de mezcla de combustibles."
                    },                  ]
            },
            {
                id: 10,
                divisionName: "División 20. Fabricación de sustancias y productos químicos",
                activities: [
                    {
                      code: 201,
                      activity: "Fabricación de sustancias químicas básicas, abonos y compuestos inorgánicos nitrogenados, plásticos y caucho sintético en formas primarias."
                    },
                    {
                      code: 2011,
                      activity: "Fabricación de sustancias y productos químicos básicos."
                    },
                    {
                      code: 2012,
                      activity: "Fabricación de abonos y compuestos inorgánicos nitrogenados."
                    },
                    {
                      code: 2013,
                      activity: "Fabricación de plásticos en formas primarias."
                    },
                    {
                      code: 2014,
                      activity: "Fabricación de caucho sintético en formas primarias."
                    },
                    {
                      code: 202,
                      activity: "Fabricación de otros productos químicos."
                    },
                    {
                      code: 2021,
                      activity: "Fabricación de plaguicidas y otros productos químicos de uso agropecuario."
                    },
                    {
                      code: 2022,
                      activity: "Fabricación de pinturas, barnices y revestimientos similares, tintas para impresión y masillas."
                    },
                    {
                      code: 2023,
                      activity: "Fabricación de jabones y detergentes, preparados para limpiar y pulir; perfumes y preparados de tocador."
                    },
                    {
                      code: 2029,
                      activity: "Fabricación de otros productos químicos n.c.p."
                    },
                    {
                      code: 203,
                      activity: "Fabricación de fibras sintéticas y artificiales."
                    },
                    {
                      code: 2030,
                      activity: "Fabricación de fibras sintéticas y artificiales."
                    },                  ]
            }, 
            {
                id: 11,
                divisionName: "División 21. Fabricación de productos farmacéuticos, sustancias químicas medicinales y productos botánicos de uso farmacéutico",
                activities: [
                    {
                      code: 210,
                      activity: "Fabricación de productos farmacéuticos, sustancias químicas medicinales y productos botánicos de uso farmacéutico."
                    },
                    {
                      code: 2100,
                      activity: "Fabricación de productos farmacéuticos, sustancias químicas medicinales y productos botánicos de uso farmacéutico."
                    },                  ]
            }, 
            {
                id: 12,
                divisionName: "División 22. Fabricación de productos de caucho y de plástico",
                activities: [
                    {
                      code: 221,
                      activity: "Fabricación de productos de caucho."
                    },
                    {
                      code: 2211,
                      activity: "Fabricación de llantas y neumáticos de caucho"
                    },
                    {
                      code: 2212,
                      activity: "Reencauche de llantas usadas"
                    },
                    {
                      code: 2219,
                      activity: "Fabricación de formas básicas de caucho y otros productos de caucho n.c.p."
                    },
                    {
                      code: 222,
                      activity: "Fabricación de productos de plástico."
                    },
                    {
                      code: 2221,
                      activity: "Fabricación de formas básicas de plástico."
                    },
                    {
                      code: 2229,
                      activity: "Fabricación de artículos de plástico n.c.p."
                    },                  ]
            },
            {
                id: 13,
                divisionName: "División 23. Fabricación de otros productos minerales no metálicos",
                activities: [
                    {
                      code: 231,
                      activity: "Fabricación de vidrio y productos de vidrio."
                    },
                    {
                      code: 2310,
                      activity: "Fabricación de vidrio y productos de vidrio."
                    },
                    {
                      code: 239,
                      activity: "Fabricación de productos minerales no metálicos n.c.p."
                    },
                    {
                      code: 2391,
                      activity: "Fabricación de productos refractarios."
                    },
                    {
                      code: 2392,
                      activity: "Fabricación de materiales de arcilla para la construcción."
                    },
                    {
                      code: 2393,
                      activity: "Fabricación de otros productos de cerámica y porcelana."
                    },
                    {
                      code: 2394,
                      activity: "Fabricación de cemento, cal y yeso."
                    },
                    {
                      code: 2395,
                      activity: "Fabricación de artículos de hormigón, cemento y yeso."
                    },
                    {
                      code: 2396,
                      activity: "Corte, tallado y acabado de la piedra."
                    },
                    {
                      code: 2399,
                      activity: "Fabricación de otros productos minerales no metálicos n.c.p."
                    },                  ]
            },
            {
                id: 14,
                divisionName: "División 24. Fabricación de productos metalúrgicos básicos",
                activities: [
                    {
                      code: 241,
                      activity: "Industrias básicas de hierro y de acero."
                    },
                    {
                      code: 2410,
                      activity: "Industrias básicas de hierro y de acero."
                    },
                    {
                      code: 242,
                      activity: "Industrias básicas de metales preciosos y de metales no ferrosos."
                    },
                    {
                      code: 2421,
                      activity: "Industrias básicas de metales preciosos."
                    },
                    {
                      code: 2429,
                      activity: "Industrias básicas de otros metales no ferrosos."
                    },
                    {
                      code: 243,
                      activity: "Fundición de metales."
                    },
                    {
                      code: 2431,
                      activity: "Fundición de hierro y de acero."
                    },
                    {
                      code: 2432,
                      activity: "Fundición de metales no ferrosos."
                    },                  ]
            },
            {
                id: 15,
                divisionName: "División 25. Fabricación de productos elaborados de metal, excepto maquinaria y equipo",
                activities: [
                    {
                      code: 251,
                      activity: "Fabricación de productos metálicos para uso estructural, tanques, depósitos y generadores de vapor."
                    },
                    {
                      code: 2511,
                      activity: "Fabricación de productos metálicos para uso estructural."
                    },
                    {
                      code: 2512,
                      activity: "Fabricación de tanques, depósitos y recipientes de metal, excepto los utilizados para el envase o transporte de mercancías."
                    },
                    {
                      code: 2513,
                      activity: "Fabricación de generadores de vapor, excepto calderas de agua caliente para calefacción central."
                    },
                    {
                      code: 252,
                      activity: "Fabricación de armas y municiones."
                    },
                    {
                      code: 2520,
                      activity: "Fabricación de armas y municiones."
                    },
                    {
                      code: 259,
                      activity: "Fabricación de otros productos elaborados de metal y actividades de servicios relacionadas con el trabajo de metales."
                    },
                    {
                      code: 2591,
                      activity: "Forja, prensado, estampado y laminado de metal; pulvimetalurgia."
                    },
                    {
                      code: 2592,
                      activity: "Tratamiento y revestimiento de metales; mecanizado."
                    },
                    {
                      code: 2593,
                      activity: "Fabricación de artículos de cuchillería, herramientas de mano y artículos de ferretería."
                    },
                    {
                      code: 2599,
                      activity: "Fabricación de otros productos elaborados de metal n.c.p."
                    },                  ]
            },
            {
                id: 16,
                divisionName: "División 26. Fabricación de productos informáticos, electrónicos y ópticos",
                activities: [
                    {
                      code: 261,
                      activity: "Fabricación de componentes y tableros electrónicos."
                    },
                    {
                      code: 2610,
                      activity: "Fabricación de componentes y tableros electrónicos."
                    },
                    {
                      code: 262,
                      activity: "Fabricación de computadoras y de equipo periférico."
                    },
                    {
                      code: 2620,
                      activity: "Fabricación de computadoras y de equipo periférico."
                    },
                    {
                      code: 263,
                      activity: "Fabricación de equipos de comunicación."
                    },
                    {
                      code: 2630,
                      activity: "Fabricación de equipos de comunicación."
                    },
                    {
                      code: 264,
                      activity: "Fabricación de aparatos electrónicos de consumo."
                    },
                    {
                      code: 2640,
                      activity: "Fabricación de aparatos electrónicos de consumo."
                    },
                    {
                      code: 265,
                      activity: "Fabricación de equipo de medición, prueba, navegación y control; fabricación de relojes."
                    },
                    {
                      code: 2651,
                      activity: "Fabricación de equipo de medición, prueba, navegación y control."
                    },
                    {
                      code: 2652,
                      activity: "Fabricación de relojes."
                    },
                    {
                      code: 266,
                      activity: "Fabricación de equipo de irradiación y equipo electrónico de uso médico y terapéutico."
                    },
                    {
                      code: 2660,
                      activity: "Fabricación de equipo de irradiación y equipo electrónico de uso médico y tera­péutico."
                    },
                    {
                      code: 267,
                      activity: "Fabricación de instrumentos ópticos y equipo fotográfico."
                    },
                    {
                      code: 2670,
                      activity: "Fabricación de instrumentos ópticos y equipo fotográfico."
                    },
                    {
                      code: 268,
                      activity: "Fabricación de medios magnéticos y ópticos para almacenamiento de datos."
                    },
                    {
                      code: 2680,
                      activity: "Fabricación de medios magnéticos y ópticos para almacenamiento de datos."
                    },                  ]
            },
            {
                id: 17,
                divisionName: "División 27. Fabricación de aparatos y equipo eléctrico",
                activities: [
                    {
                      code: 271,
                      activity: "Fabricación de motores, generadores y transformadores eléctricos y de aparatos de distribución y control de la energía eléctrica."
                    },
                    {
                      code: 2711,
                      activity: "Fabricación de motores, generadores y transformadores eléctricos."
                    },
                    {
                      code: 2712,
                      activity: "Fabricación de aparatos de distribución y control de la energía eléctrica."
                    },
                    {
                      code: 272,
                      activity: "Fabricación de pilas, baterías y acumuladores eléctricos."
                    },
                    {
                      code: 2720,
                      activity: "Fabricación de pilas, baterías y acumuladores eléctricos."
                    },
                    {
                      code: 273,
                      activity: "Fabricación de hilos y cables aislados y sus dispositivos."
                    },
                    {
                      code: 2731,
                      activity: "Fabricación de hilos y cables eléctricos y de fibra óptica."
                    },
                    {
                      code: 2732,
                      activity: "Fabricación de dispositivos de cableado."
                    },
                    {
                      code: 274,
                      activity: "Fabricación de equipos eléctricos de iluminación."
                    },
                    {
                      code: 2740,
                      activity: "Fabricación de equipos eléctricos de iluminación."
                    },
                    {
                      code: 275,
                      activity: "Fabricación de aparatos de uso doméstico."
                    },
                    {
                      code: 2750,
                      activity: "Fabricación de aparatos de uso doméstico."
                    },
                    {
                      code: 279,
                      activity: "Fabricación de otros tipos de equipo eléctrico n.c.p."
                    },
                    {
                      code: 2790,
                      activity: "Fabricación de otros tipos de equipo eléctrico n.c.p."
                    },                  ]
            },
            {
                id: 18,
                divisionName: "División 28. Fabricación de maquinaria y equipo n.c.p",
                activities: [
                    {
                      code: 281,
                      activity: "Fabricación de maquinaria y equipo de uso general."
                    },
                    {
                      code: 2811,
                      activity: "Fabricación de motores, turbinas, y partes para motores de combustión interna."
                    },
                    {
                      code: 2812,
                      activity: "Fabricación de equipos de potencia hidráulica y neumática."
                    },
                    {
                      code: 2813,
                      activity: "Fabricación de otras bombas, compresores, grifos y válvulas."
                    },
                    {
                      code: 2814,
                      activity: "Fabricación de cojinetes, engranajes, trenes de engranajes y piezas de transmisión."
                    },
                    {
                      code: 2815,
                      activity: "Fabricación de hornos, hogares y quemadores industriales."
                    },
                    {
                      code: 2816,
                      activity: "Fabricación de equipo de elevación y manipulación."
                    },
                    {
                      code: 2817,
                      activity: "Fabricación de maquinaria y equipo de oficina (excepto computadoras y equipo periférico)."
                    },
                    {
                      code: 2818,
                      activity: "Fabricación de herramientas manuales con motor."
                    },
                    {
                      code: 2819,
                      activity: "Fabricación de otros tipos de maquinaria y equipo de uso general n.c.p."
                    },
                    {
                      code: 282,
                      activity: "Fabricación de maquinaria y equipo de uso especial."
                    },
                    {
                      code: 2821,
                      activity: "Fabricación de maquinaria agropecuaria y forestal."
                    },
                    {
                      code: 2822,
                      activity: "Fabricación de máquinas formadoras de metal y de máquinas herramienta."
                    },
                    {
                      code: 2823,
                      activity: "Fabricación de maquinaria para la metalurgia."
                    },
                    {
                      code: 2824,
                      activity: "Fabricación de maquinaria para explotación de minas y canteras y para obras de construcción."
                    },
                    {
                      code: 2825,
                      activity: "Fabricación de maquinaria para la elaboración de alimentos, bebidas y tabaco."
                    },
                    {
                      code: 2826,
                      activity: "Fabricación de maquinaria para la elaboración de productos textiles, prendas de vestir y cueros."
                    },
                    {
                      code: 2829,
                      activity: "Fabricación de otros tipos de maquinaria y equipo de uso especial n.c.p."
                    },                  ]
            },
            {
                id: 19,
                divisionName: "División 29. Fabricación de vehículos automotores, remolques y semirremolques",
                activities: [
                    {
                      code: 291,
                      activity: "Fabricación de vehículos automotores y sus motores."
                    },
                    {
                      code: 2910,
                      activity: "Fabricación de vehículos automotores y sus motores."
                    },
                    {
                      code: 292,
                      activity: "Fabricación de carrocerías para vehículos automotores; fabricación de remolques y semirremolques."
                    },
                    {
                      code: 2920,
                      activity: "Fabricación de carrocerías para vehículos automotores; fabricación de remolques y semirremolques."
                    },
                    {
                      code: 293,
                      activity: "Fabricación de partes, piezas (autopartes) y accesorios (lujos) para vehículos automotores."
                    },
                    {
                      code: 2930,
                      activity: "Fabricación de partes, piezas (autopartes) y accesorios (lujos) para vehículos automotores."
                    },                  ]
            },
            {
                id: 20,
                divisionName: "División 30. Fabricación de otros tipos de equipo de transporte",
                activities: [
                    {
                      code: 301,
                      activity: "Construcción de barcos y otras embarcaciones."
                    },
                    {
                      code: 3011,
                      activity: "Construcción de barcos y de estructuras flotantes."
                    },
                    {
                      code: 3012,
                      activity: "Construcción de embarcaciones de recreo y deporte."
                    },
                    {
                      code: 302,
                      activity: "Fabricación de locomotoras y de material rodante para ferrocarriles."
                    },
                    {
                      code: 3020,
                      activity: "Fabricación de locomotoras y de material rodante para ferrocarriles."
                    },
                    {
                      code: 303,
                      activity: "Fabricación de aeronaves, naves espaciales y de maquinaria conexa."
                    },
                    {
                      code: 3030,
                      activity: "Fabricación de aeronaves, naves espaciales y de maquinaria conexa."
                    },
                    {
                      code: 304,
                      activity: "Fabricación de vehículos militares de combate."
                    },
                    {
                      code: 3040,
                      activity: "Fabricación de vehículos militares de combate."
                    },
                    {
                      code: 309,
                      activity: "Fabricación de otros tipos de equipo de transporte n.c.p."
                    },
                    {
                      code: 3091,
                      activity: "Fabricación de motocicletas."
                    },
                    {
                      code: 3092,
                      activity: "Fabricación de bicicletas y de sillas de ruedas para personas con discapacidad."
                    },
                    {
                      code: 3099,
                      activity: "Fabricación de otros tipos de equipo de transporte n.c.p."
                    },                  ]
            },
            {
                id: 21,
                divisionName: "División 31. Fabricación de muebles, colchones y somieres",
                activities: [
                    {
                      code: 311,
                      activity: "Fabricación de muebles."
                    },
                    {
                      code: 3110,
                      activity: "Fabricación de muebles."
                    },
                    {
                      code: 312,
                      activity: "Fabricación de colchones y somieres."
                    },
                    {
                      code: 3120,
                      activity: "Fabricación de colchones y somieres."
                    },                  ]
            },
            {
                id: 22,
                divisionName: "División 32. Otras industrias manufactureras",
                activities: [
                    {
                      code: 321,
                      activity: "Fabricación de joyas, bisutería y artículos conexos."
                    },
                    {
                      code: 3210,
                      activity: "Fabricación de joyas, bisutería y artículos conexos."
                    },
                    {
                      code: 322,
                      activity: "Fabricación de instrumentos musicales."
                    },
                    {
                      code: 3220,
                      activity: "Fabricación de instrumentos musicales."
                    },
                    {
                      code: 323,
                      activity: "Fabricación de artículos y equipo para la práctica del deporte."
                    },
                    {
                      code: 3230,
                      activity: "Fabricación de artículos y equipo para la práctica del deporte."
                    },
                    {
                      code: 324,
                      activity: "Fabricación de juegos, juguetes y rompecabezas."
                    },
                    {
                      code: 3240,
                      activity: "Fabricación de juegos, juguetes y rompecabezas."
                    },
                    {
                      code: 325,
                      activity: "Fabricación de instrumentos, aparatos y materiales médicos y odontológicos (incluido mobiliario)."
                    },
                    {
                      code: 3250,
                      activity: "Fabricación de instrumentos, aparatos y materiales médicos y odontológicos (incluido mobiliario)."
                    },
                    {
                      code: 329,
                      activity: "Otras industrias manufactureras n.c.p."
                    },
                    {
                      code: 3290,
                      activity: "Otras industrias manufactureras n.c.p."
                    },                  ]
            },
            {
                id: 23,
                divisionName: "División 33. Instalación, mantenimiento y reparación especializada de maquinaria y equipo",
                activities: [
                    {
                      code: 331,
                      activity: "Mantenimiento y reparación especializado de productos elaborados en metal y de maquinaria y equipo."
                    },
                    {
                      code: 3311,
                      activity: "Mantenimiento y reparación especializado de productos elaborados en metal."
                    },
                    {
                      code: 3312,
                      activity: "Mantenimiento y reparación especializado de maquinaria y equipo."
                    },
                    {
                      code: 3313,
                      activity: "Mantenimiento y reparación especializado de equipo electrónico y óptico."
                    },
                    {
                      code: 3314,
                      activity: "Mantenimiento y reparación especializado de equipo eléctrico."
                    },
                    {
                      code: 3315,
                      activity: "Mantenimiento y reparación especializado de equipo de transporte, excepto los vehículos automotores, motocicletas y bicicletas."
                    },
                    {
                      code: 3319,
                      activity: "Mantenimiento y reparación de otros tipos de equipos y sus componentes n.c.p."
                    },
                    {
                      code: 332,
                      activity: "Instalación especializada de maquinaria y equipo industrial."
                    },
                    {
                      code: 3320,
                      activity: "Instalación especializada de maquinaria y equipo industrial."
                    },                  ]
            },
            
        ]
    },
    {
      id: 3,
      sectionName: "Sección D. Suministro de Electricidad, Gas, Vapor y Aire acondicionado",
      divisions: [
          {
              id: 0,
              divisionName: "División 35. Suministro de electricidad, gas, vapor y aire acondicionado",
              activities: [
                  {
                    code: 351,
                    activity: "Generación, transmisión, distribución y comercialización de energía eléctrica.",
                  },
                  {
                    code: 3511,
                    activity: "Generación de energía eléctrica.",
                  },
                  {
                    code: 3512,
                    activity: "Transmisión de energía eléctrica.",
                  },
                  {
                    code: 3513,
                    activity: "Distribución de energía eléctrica.",
                  },
                  {
                    code: 3514,
                    activity: "Comercialización de energía eléctrica.",
                  },
                  {
                    code: 352,
                    activity: "Producción de gas; distribución de combustibles gaseosos por tuberías.",
                  },
                  {
                    code: 3520,
                    activity: "Producción de gas; distribución de combustibles gaseosos por tuberías.",
                  },
                  {
                    code: 353,
                    activity: "Suministro de vapor y aire acondicionado.",
                  },
                  {
                    code: 3530,
                    activity: "Suministro de vapor y aire acondicionado.",
                  }
                ]
          },
      ]
    },
    {
      id: 4,
      sectionName: "Sección E. Distribución de Agua; Evacuación y Tratamiento de Aguas Residuales, Gestión de Desechos y Actividades de Saneamiento Ambiental",
      divisions: [
          {
              id: 0,
              divisionName: "División 36. Captación, tratamiento y distribución de agua",
              activities: [
                  {
                    code: 360,
                    activity: "Captación, tratamiento y distribución de agua.",
                  }
                ]
          },
          {
              id: 1,
              divisionName: "División 37. Evacuación y tratamiento de aguas residuales",
              activities: [
                  {
                    code: 370,
                    activity: "Evacuación y tratamiento de aguas residuales.",
                  },
                  {
                    code: 3700,
                    activity: "Evacuación y tratamiento de aguas residuales.",
                  }
                ]
          },
          {
              id: 2,
              divisionName: "División 38. Recolección, tratamiento y disposición de desechos, recuperación de materiales",
              activities: [
                  {
                    code: 381,
                    activity: "Recolección de desechos.",
                  },
                  {
                    code: 3811,
                    activity: "Recolección de desechos no peligrosos.",
                  },
                  {
                    code: 3812,
                    activity: "Recolección de desechos peligrosos.",
                  },
                  {
                    code: 382,
                    activity: "Tratamiento y disposición de desechos.",
                  },
                  {
                    code: 3821,
                    activity: "Tratamiento y disposición de desechos no peligrosos.",
                  },
                  {
                    code: 3822,
                    activity: "Tratamiento y disposición de desechos peligrosos.",
                  },
                  {
                    code: 383,
                    activity: "Recuperación de materiales.",
                  },
                  {
                    code: 3830,
                    activity: "Recuperación de materiales.",
                  }
                ]
          },
          {
              id: 3,
              divisionName: "División 39. Actividades de saneamiento ambiental y otros servicios de gestión de desechos",
              activities: [
                  {
                    code: 390,
                    activity: "Actividades de saneamiento ambiental y otros servicios de gestión de desechos.",
                  },
                  {
                    code: 3900,
                    activity: "Actividades de saneamiento ambiental y otros servicios de gestión de desechos.",
                  }
                ]
          },
      ]
    },
    {
      id: 5,
      sectionName: "Sección F. Construcción",
      divisions: [
          {
              id: 0,
              divisionName: "División 41. Construcción de edificios",
              activities: [
                  {
                    code: 411,
                    activity: "Construcción de edificios.",
                  },
                  {
                    code: 4111,
                    activity: "Construcción de edificios residenciales.",
                  },
                  {
                    code: 4112,
                    activity: "Construcción de edificios no residenciales.",
                  }
                ]
          },
          {
              id: 1,
              divisionName: "División 42. Obras de ingeniería civil",
              activities: [
                  {
                    code: 421,
                    activity: "Construcción de carreteras y vías de ferrocarril.",
                  },
                  {
                    code: 4210,
                    activity: "Construcción de carreteras y vías de ferrocarril.",
                  },
                  {
                    code: 422,
                    activity: "Construcción de proyectos de servicio público.",
                  },
                  {
                    code: 4220,
                    activity: "Construcción de proyectos de servicio público.",
                  },
                  {
                    code: 429,
                    activity: "Construcción de otras obras de ingeniería civil.",
                  },
                  {
                    code: 4290,
                    activity: "Construcción de otras obras de ingeniería civil.",
                  }
                ]
          },
          {
              id: 2,
              divisionName: "División 43. Actividades especializadas para la construcción de edificios y obras de ingeniería civil",
              activities: [
                  {
                    code: 431,
                    activity: "Demolición y preparación del terreno.",
                  },
                  {
                    code: 4311,
                    activity: "Demolición.",
                  },
                  {
                    code: 4312,
                    activity: "Preparación del terreno.",
                  },
                  {
                    code: 432,
                    activity: "Instalaciones eléctricas, de fontanería y otras instalaciones especializadas.",
                  },
                  {
                    code: 4321,
                    activity: "Instalaciones eléctricas.",
                  },
                  {
                    code: 4322,
                    activity: "Instalaciones de fontanería, calefacción y aire acondicionado.",
                  },
                  {
                    code: 4329,
                    activity: "Otras instalaciones especializadas.",
                  },
                  {
                    code: 433,
                    activity: "Terminación y acabado de edificios y obras de ingeniería civil.",
                  },
                  {
                    code: 4330,
                    activity: "Terminación y acabado de edificios y obras de ingeniería civil.",
                  },
                  {
                    code: 439,
                    activity: "Otras actividades especializadas para la construcción de edificios y obras de ingeniería civil.",
                  },
                  {
                    code: 4390,
                    activity: "Otras actividades especializadas para la construcción de edificios y obras de ingeniería civil.",
                  }
                ]
          },
      ]
    },
    {
      id: 6,
      sectionName: "Sección G. Comercio al por mayor y al por menor; Reparación de vehículos Automotores y Motocicletas ",
      divisions: [
          {
              id: 0,
              divisionName: "División 45. Comercio, mantenimiento y reparación de vehículos automotores y motocicletas, sus partes, piezas y accesorios",
              activities: [
                  {
                    code: 451,
                    activity: "Comercio de vehículos automotores.",
                  },
                  {
                    code: 4511,
                    activity: "Comercio de vehículos automotores nuevos.",
                  },
                  {
                    code: 4512,
                    activity: "Comercio de vehículos automotores usados.",
                  },
                  {
                    code: 452,
                    activity: "Mantenimiento y reparación de vehículos automotores.",
                  },
                  {
                    code: 4520,
                    activity: "Mantenimiento y reparación de vehículos automotores.",
                  },
                  {
                    code: 453,
                    activity: "Comercio de partes, piezas (autopartes) y accesorios (lujos) para vehículos automotores.",
                  },
                  {
                    code: 4530,
                    activity: "Comercio de partes, piezas (autopartes) y accesorios (lujos) para vehículos au­tomotores.",
                  },
                  {
                    code: 454,
                    activity: "Comercio, mantenimiento y reparación de motocicletas y de sus partes, piezas y accesorios.",
                  },
                  {
                    code: 4541,
                    activity: "Comercio de motocicletas y de sus partes, piezas y accesorios.",
                  },
                  {
                    code: 4542,
                    activity: "Mantenimiento y reparación de motocicletas y de sus partes y piezas.",
                  }
                ]
          },
          {
              id: 1,
              divisionName: "División 46 Comercio al por mayor y en comisión o por contrata, excepto el comercio de vehículos automotores y motocicletas",
            activities:[
              {
                code: 461,
                activity: "Comercio al por mayor a cambio de una retribución o por contrata.",
              },
              {
                code: 4610,
                activity: "Comercio al por mayor a cambio de una retribución o por contrata.",
              },
              {
                code: 462,
                activity: "Comercio al por mayor de materias primas agropecuarias; animales vivos.",
              },
              {
                code: 4620,
                activity: "Comercio al por mayor de materias primas agropecuarias; animales vivos.",
              },
              {
                code: 463,
                activity: "Comercio al por mayor de alimentos, bebidas y tabaco.",
              },
              {
                code: 4631,
                activity: "Comercio al por mayor de productos alimenticios.",
              },
              {
                code: 4632,
                activity: "Comercio al por mayor de bebidas y tabaco.",
              },
              {
                code: 464,
                activity: "Comercio al por mayor de artículos y enseres domésticos (incluidas prendas de vestir).",
              },
              {
                code: 4641,
                activity: "Comercio al por mayor de productos textiles, productos confeccionados para uso doméstico.",
              },
              {
                code: 4642,
                activity: "Comercio al por mayor de prendas de vestir.",
              },
              {
                code: 4643,
                activity: "Comercio al por mayor de calzado.",
              },
              {
                code: 4644,
                activity: "Comercio al por mayor de aparatos y equipo de uso doméstico.",
              },
              {
                code: 4645,
                activity: "Comercio al por mayor de productos farmacéuticos, medicinales, cosméticos y de tocador.",
              },
              {
                code: 4649,
                activity: "Comercio al por mayor de otros utensilios domésticos n.c.p.",
              },
              {
                code: 465,
                activity: "Comercio al por mayor de maquinaria y equipo.",
              },
              {
                code: 4651,
                activity: "Comercio al por mayor de computadores, equipo periférico y programas de informática.",
              },
              {
                code: 4652,
                activity: "Comercio al por mayor de equipo, partes y piezas electrónicos y de telecomuni­caciones.",
              },
              {
                code: 4653,
                activity: "Comercio al por mayor de maquinaria y equipo agropecuarios.",
              },
              {
                code: 4659,
                activity: "Comercio al por mayor de otros tipos de maquinaria y equipo n.c.p.",
              },
              {
                code: 466,
                activity: "Comercio al por mayor especializado de otros productos.",
              },
              {
                code: 4661,
                activity: "Comercio al por mayor de combustibles sólidos, líquidos, gaseosos y productos conexos.",
              },
              {
                code: 4662,
                activity: "Comercio al por mayor de metales y productos metalíferos.",
              },
              {
                code: 4663,
                activity: "Comercio al por mayor de materiales de construcción, artículos de ferretería, pinturas, productos de vidrio, equipo y materiales de fontanería y calefacción.",
              },
              {
                code: 4664,
                activity: "Comercio al por mayor de productos químicos básicos, cauchos y plásticos en formas primarias y productos químicos de uso agropecuario.",
              },
              {
                code: 4665,
                activity: "Comercio al por mayor de desperdicios, desechos y chatarra.",
              },
              {
                code: 4669,
                activity: "Comercio al por mayor de otros productos n.c.p.",
              },
              {
                code: 469,
                activity: "Comercio al por mayor no especializado.",
              },
              {
                code: 4690,
                activity: "Comercio al por mayor no especializado.",
              }
            ]
          },
          {
              id: 2,
              divisionName: "División 47. Comercio al por menor (incluso el comercio al por menor de combus­tibles), excepto el de vehículos automotores y motocicletas",
              activities: [
                  {
                    code: 471,
                    activity: "Comercio al por menor en establecimientos no especializados.",
                  },
                  {
                    code: 4711,
                    activity: "Comercio al por menor en establecimientos no especializados con surtido compuesto principalmente por alimentos, bebidas o tabaco.",
                  },
                  {
                    code: 4719,
                    activity: "Comercio al por menor en establecimientos no especializados, con surtido compuesto principalmente por productos diferentes de alimentos (víveres en general), bebidas y tabaco.",
                  },
                  {
                    code: 472,
                    activity: "Comercio al por menor de alimentos (víveres en general), bebidas y tabaco, en establecimientos especializados.",
                  },
                  {
                    code: 4721,
                    activity: "Comercio al por menor de productos agrícolas para el consumo en establecimientos especializados.",
                  },
                  {
                    code: 4722,
                    activity: "Comercio al por menor de leche, productos lácteos y huevos, en establecimientos especializados.",
                  },
                  {
                    code: 4723,
                    activity: "Comercio al por menor de carnes (incluye aves de corral), productos cárnicos, pescados y productos de mar, en establecimientos especializados.",
                  },
                  {
                    code: 4724,
                    activity: "Comercio al por menor de bebidas y productos del tabaco, en establecimientos especializados.",
                  },
                  {
                    code: 4729,
                    activity: "Comercio al por menor de otros productos alimenticios n.c.p., en establecimientos especializados.",
                  },
                  {
                    code: 473,
                    activity: "Comercio al por menor de combustible, lubricantes, aditivos y productos de limpieza para automotores, en establecimientos especializados.",
                  },
                  {
                    code: 4731,
                    activity: "Comercio al por menor de combustible para automotores.",
                  },
                  {
                    code: 4732,
                    activity: "Comercio al por menor de lubricantes (aceites, grasas), aditivos y productos de limpieza para vehículos automotores.",
                  },
                  {
                    code: 474,
                    activity: "Comercio al por menor de equipos de informática y de comunicaciones, en establecimientos especializados.",
                  },
                  {
                    code: 4741,
                    activity: "Comercio al por menor de computadores, equipos periféricos, programas de in­formática y equipos de telecomunicaciones en establecimientos especializados.",
                  },
                  {
                    code: 4742,
                    activity: "Comercio al por menor de equipos y aparatos de sonido y de video, en establecimientos especializados.",
                  },
                  {
                    code: 475,
                    activity: "Comercio al por menor de otros enseres domésticos en establecimientos es­pecializados.",
                  },
                  {
                    code: 4751,
                    activity: "Comercio al por menor de productos textiles en establecimientos especializados.",
                  },
                  {
                    code: 4752,
                    activity: "Comercio al por menor de artículos de ferretería, pinturas y productos de vidrio en establecimientos especializados.",
                  },
                  {
                    code: 4753,
                    activity: "Comercio al por menor de tapices, alfombras y cubrimientos para paredes y pisos en establecimientos especializados.",
                  },
                  {
                    code: 4754,
                    activity: "Comercio al por menor de electrodomésticos y gasodomésticos de uso doméstico, muebles y equipos de iluminación.",
                  },
                  {
                    code: 4755,
                    activity: "Comercio al por menor de artículos y utensilios de uso doméstico.",
                  },
                  {
                    code: 4759,
                    activity: "Comercio al por menor de otros artículos domésticos en establecimientos espe­cializados.",
                  },
                  {
                    code: 476,
                    activity: "Comercio al por menor de artículos culturales y de entretenimiento, en esta­blecimientos especializados.",
                  },
                  {
                    code: 4761,
                    activity: "Comercio al por menor de libros, periódicos, materiales y artículos de papelería y escritorio, en establecimientos especializados.",
                  },
                  {
                    code: 4762,
                    activity: "Comercio al por menor de artículos deportivos, en establecimientos especializados.",
                  },
                  {
                    code: 4769,
                    activity: "Comercio al por menor de otros artículos culturales y de entretenimiento n.c.p. en establecimientos especializados.",
                  },
                  {
                    code: 477,
                    activity: "Comercio al por menor de otros productos en establecimientos especializados.",
                  },
                  {
                    code: 4771,
                    activity: "Comercio al por menor de prendas de vestir y sus accesorios (incluye artículos de piel) en establecimientos especializados.",
                  },
                  {
                    code: 4772,
                    activity: "Comercio al por menor de todo tipo de calzado y artículos de cuero y sucedáneos del cuero en establecimientos especializados.",
                  },
                  {
                    code: 4773,
                    activity: "Comercio al por menor de productos farmacéuticos y medicinales, cosméticos y artículos de tocador en establecimientos especializados.",
                  },
                  {
                    code: 4774,
                    activity: "Comercio al por menor de otros productos nuevos en establecimientos especializados.",
                  },
                  {
                    code: 4775,
                    activity: "Comercio al por menor de artículos de segunda mano.",
                  },
                  {
                    code: 478,
                    activity: "Comercio al por menor en puestos de venta móviles.",
                  },
                  {
                    code: 4781,
                    activity: "Comercio al por menor de alimentos, bebidas y tabaco, en puestos de venta móviles.",
                  },
                  {
                    code: 4782,
                    activity: "Comercio al por menor de productos textiles, prendas de vestir y calzado, en puestos de venta móviles.",
                  },
                  {
                    code: 4789,
                    activity: "Comercio al por menor de otros productos en puestos de venta móviles.",
                  },
                  {
                    code: 479,
                    activity: "Comercio al por menor no realizado en establecimientos, puestos de venta o mercados.",
                  },
                  {
                    code: 4791,
                    activity: "Comercio al por menor realizado a través de internet.",
                  },
                  {
                    code: 4792,
                    activity: "Comercio al por menor realizado a través de casas de venta o por correo.",
                  },
                  {
                    code: 4799,
                    activity: "Otros tipos de comercio al por menor no realizado en establecimientos, puestos de venta o mercados.",
                  }
                ]
          },
      ]
    },
    {
      id: 7,
      sectionName: "Sección H. Transporte y Almacenamiento",
      divisions: [
          {
              id: 0,
              divisionName: "División 49. Transporte terrestre; transporte por tuberías",
              activities: [
                  {
                    code: 491,
                    activity: "Transporte férreo.",
                  },
                  {
                    code: 4911,
                    activity: "Transporte férreo de pasajeros.",
                  },
                  {
                    code: 4912,
                    activity: "Transporte férreo de carga.",
                  },
                  {
                    code: 492,
                    activity: "Transporte terrestre público automotor.",
                  },
                  {
                    code: 4921,
                    activity: "Transporte de pasajeros.",
                  },
                  {
                    code: 4922,
                    activity: "Transporte mixto.",
                  },
                  {
                    code: 4923,
                    activity: "Transporte de carga por carretera.",
                  },
                  {
                    code: 493,
                    activity: "Transporte por tuberías.",
                  },
                  {
                    code: 4930,
                    activity: "Transporte por tuberías.",
                  }
                ]
          },
          {
              id: 1,
              divisionName: "División 50. Transporte acuático",
              activities: [
                  {
                    code: 501,
                    activity: "Transporte marítimo y de cabotaje.",
                  },
                  {
                    code: 5011,
                    activity: "Transporte de pasajeros marítimo y de cabotaje.",
                  },
                  {
                    code: 5012,
                    activity: "Transporte de carga marítimo y de cabotaje.",
                  },
                  {
                    code: 502,
                    activity: "Transporte fluvial.",
                  },
                  {
                    code: 5021,
                    activity: "Transporte fluvial de pasajeros.",
                  },
                  {
                    code: 5022,
                    activity: "Transporte fluvial de carga.",
                  }
                ]
          },
          {
              id: 2,
              divisionName: "División 51. Transporte aéreo",
              activities: [
                  {
                    code: 511,
                    activity: "Transporte aéreo de pasajeros.",
                  },
                  {
                    code: 5111,
                    activity: "Transporte aéreo nacional de pasajeros.",
                  },
                  {
                    code: 5112,
                    activity: "Transporte aéreo internacional de pasajeros.",
                  },
                  {
                    code: 512,
                    activity: "Transporte aéreo de carga.",
                  },
                  {
                    code: 5121,
                    activity: "Transporte aéreo nacional de carga.",
                  },
                  {
                    code: 5122,
                    activity: "Transporte aéreo internacional de carga.",
                  }
                ]
          },
          {
              id: 3,
              divisionName: "División 52. Almacenamiento y actividades complementarias al transporte",
              activities: [
                  {
                    code: 521,
                    activity: "Almacenamiento y depósito.",
                  },
                  {
                    code: 5210,
                    activity: "Almacenamiento y depósito.",
                  },
                  {
                    code: 522,
                    activity: "Actividades de las estaciones, vías y servicios complementarios para el transporte.",
                  },
                  {
                    code: 5221,
                    activity: "Actividades de estaciones, vías y servicios complementarios para el transporte terrestre.",
                  },
                  {
                    code: 5222,
                    activity: "Actividades de puertos y servicios complementarios para el transporte acuático.",
                  },
                  {
                    code: 5223,
                    activity: "Actividades de aeropuertos, servicios de navegación aérea y demás actividades conexas al transporte aéreo.",
                  },
                  {
                    code: 5224,
                    activity: "Manipulación de carga.",
                  },
                  {
                    code: 5229,
                    activity: "Otras actividades complementarias al transporte.",
                  }
                ]
          },
          {
              id: 4,
              divisionName: "División 53. Correo y servicios de mensajería",
              activities: [
                  {
                    code: 531,
                    activity: "Actividades postales nacionales.",
                  },
                  {
                    code: 5310,
                    activity: "Actividades postales nacionales.",
                  },
                  {
                    code: 532,
                    activity: "Actividades de mensajería.",
                  },
                  {
                    code: 5320,
                    activity: "Actividades de mensajería.",
                  }
                ]
          },
      ]
    },
    {
      id: 8,
      sectionName: "Sección I. Alojamiento y servicios de comida",
      divisions: [
          {
              id: 0,
              divisionName: "División 55. Alojamiento",
              activities: [
                  {
                    code: 551,
                    activity: "Actividades de alojamiento de estancias cortas.",
                  },
                  {
                    code: 5511,
                    activity: "Alojamiento en hoteles.",
                  },
                  {
                    code: 5512,
                    activity: "Alojamiento en apartahoteles.",
                  },
                  {
                    code: 5513,
                    activity: "Alojamiento en centros vacacionales.",
                  },
                  {
                    code: 5514,
                    activity: "Alojamiento rural.",
                  },
                  {
                    code: 5519,
                    activity: "Otros tipos de alojamientos para visitantes.",
                  },
                  {
                    code: 552,
                    activity: "Actividades de zonas de camping y parques para vehículos recreacionales.",
                  },
                  {
                    code: 5520,
                    activity: "Actividades de zonas de camping y parques para vehículos recreacionales.",
                  },
                  {
                    code: 553,
                    activity: "Servicio por horas.",
                  },
                  {
                    code: 5530,
                    activity: "Servicio por horas",
                  },
                  {
                    code: 559,
                    activity: "Otros tipos de alojamiento n.c.p.",
                  },
                  {
                    code: 5590,
                    activity: "Otros tipos de alojamiento n.c.p.",
                  }
                ]
          },
          {
              id: 1,
              divisionName: "División 56. Actividades de servicios de comidas y bebidas",
              activities: [
                  {
                    code: 561,
                    activity: "Actividades de restaurantes, cafeterías y servicio móvil de comidas.",
                  },
                  {
                    code: 5611,
                    activity: "Expendio a la mesa de comidas preparadas.",
                  },
                  {
                    code: 5612,
                    activity: "Expendio por autoservicio de comidas preparadas.",
                  },
                  {
                    code: 5613,
                    activity: "Expendio de comidas preparadas en cafeterías.",
                  },
                  {
                    code: 5619,
                    activity: "Otros tipos de expendio de comidas preparadas n.c.p.",
                  },
                  {
                    code: 562,
                    activity: "Actividades de catering para eventos y otros servicios de comidas.",
                  },
                  {
                    code: 5621,
                    activity: "Catering para eventos.",
                  },
                  {
                    code: 5629,
                    activity: "Actividades de otros servicios de comidas.",
                  },
                  {
                    code: 563,
                    activity: "Expendio de bebidas alcohólicas para el consumo dentro del establecimiento.",
                  },
                  {
                    code: 5630,
                    activity: "Expendio de bebidas alcohólicas para el consumo dentro del establecimiento.",
                  }
                ]
          },
      ]
    },
    {
      id: 9,
      sectionName: "SECCIÓN J. INFORMACIÓN Y COMUNICACIONES",
      divisions: [
          {
              id: 0,
              divisionName: "División 58. Actividades de edición",
              activities: [
                  {
                    code: 581,
                    activity: "Edición de libros, publicaciones periódicas y otras actividades de edición.",
                  },
                  {
                    code: 5811,
                    activity: "Edición de libros.",
                  },
                  {
                    code: 5812,
                    activity: "Edición de directorios y listas de correo.",
                  },
                  {
                    code: 5813,
                    activity: "Edición de periódicos, revistas y otras publicaciones periódicas.",
                  },
                  {
                    code: 5819,
                    activity: "Otros trabajos de edición.",
                  },
                  {
                    code: 582,
                    activity: "Edición de programas de informática (software).",
                  },
                  {
                    code: 5820,
                    activity: "Edición de programas de informática (software).",
                  }
                ]
          },
          {
              id: 1,
              divisionName: "División 59. Actividades cinematográficas, de video y producción de programas de televisión, grabación de sonido y edición de música",
              activities: [
                  {
                    code: 591,
                    activity: "Actividades de producción de películas cinematográficas, video y producción de programas, anuncios y comerciales de televisión.",
                  },
                  {
                    code: 5911,
                    activity: "Actividades de producción de películas cinematográficas, videos, programas, anuncios y comerciales de televisión.",
                  },
                  {
                    code: 5912,
                    activity: "Actividades de posproducción de películas cinematográficas, videos, programas, anuncios y comerciales de televisión.",
                  },
                  {
                    code: 5913,
                    activity: "Actividades de distribución de películas cinematográficas, videos, programas, anuncios y comerciales de televisión.",
                  },
                  {
                    code: 5914,
                    activity: "Actividades de exhibición de películas cinematográficas y videos.",
                  },
                  {
                    code: 592,
                    activity: "Actividades de grabación de sonido y edición de música.",
                  },
                  {
                    code: 5920,
                    activity: "Actividades de grabación de sonido y edición de música.",
                  }
                ]
          },
          {
              id: 2,
              divisionName: "División 60. Actividades de programación, transmisión y difusión",
              activities: [
    {
      code: 601,
      activity: "Actividades de programación y transmisión en el servicio de radiodifusión sonora.",
    },
    {
      code: 6010,
      activity: "Actividades de programación y transmisión en el servicio de radiodifusión sonora.",
    },
    {
      code: 602,
      activity: "Actividades de programación y transmisión de televisión.",
    },
    {
      code: 6020,
      activity: "Actividades de programación y transmisión de televisión.",
    }
  ]
  
          },
          {
              id: 3,
              divisionName: "División 61. Telecomunicaciones",
              activities: [
                  {
                    code: 611,
                    activity: "Actividades de telecomunicaciones alámbricas.",
                  },
                  {
                    code: 6110,
                    activity: "Actividades de telecomunicaciones alámbricas.",
                  },
                  {
                    code: 612,
                    activity: "Actividades de telecomunicaciones inalámbricas.",
                  },
                  {
                    code: 6120,
                    activity: "Actividades de telecomunicaciones inalámbricas.",
                  },
                  {
                    code: 613,
                    activity: "Actividades de telecomunicación satelital.",
                  },
                  {
                    code: 6130,
                    activity: "Actividades de telecomunicación satelital.",
                  },
                  {
                    code: 619,
                    activity: "Otras actividades de telecomunicaciones.",
                  },
                  {
                    code: 6190,
                    activity: "Otras actividades de telecomunicaciones.",
                  }
                ]
          },
          {
              id: 4,
              divisionName: "División 62. Desarrollo de sistemas informáticos (planificación, análisis, diseño, programación, pruebas), consultoría informática y actividades relacionadas",
              activities: [
                  {
                    code: 620,
                    activity: "Desarrollo de sistemas informáticos (planificación, análisis, diseño, programa­ción, pruebas), consultoría informática y actividades relacionadas.",
                  },
                  {
                    code: 6201,
                    activity: "Actividades de desarrollo de sistemas informáticos (planificación, análisis, diseño, programación, pruebas).",
                  },
                  {
                    code: 6202,
                    activity: "Actividades de consultoría informática y actividades de administración de insta­laciones informáticas.",
                  },
                  {
                    code: 6209,
                    activity: "Otras actividades de tecnologías de información y actividades de servicios in­formáticos.",
                  }
                ]
          },
          {
              id: 5,
              divisionName: "División 63. Actividades de servicios de información",
              activities: [
                  {
                    code: 631,
                    activity: "Procesamiento de datos, alojamiento (hosting) y actividades relacionadas; portales web.",
                  },
                  {
                    code: 6311,
                    activity: "Procesamiento de datos, alojamiento (hosting) y actividades relacionadas.",
                  },
                  {
                    code: 6312,
                    activity: "Portales web.",
                  },
                  {
                    code: 639,
                    activity: "Otras actividades de servicio de información.",
                  },
                  {
                    code: 6391,
                    activity: "Actividades de agencias de noticias.",
                  },
                  {
                    code: 6399,
                    activity: "Otras actividades de servicio de información n.c.p.",
                  }
                ]
          },
      ]
    }
]