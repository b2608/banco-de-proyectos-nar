export const Lines = 
    [{
        lineId: 0,
        lineName: "Línea 1. Soberania y Autonomía Alimentaria",
        lines: [
            {
              id: 1,
              line: "Modelo de desarrollo endógeno basado en la economía familiar orientada a la producción diversificada para el autoconsumo y el abastecimiento."
            },
            {
              id: 2,
              line: "Fortalecimiento de espacios para la participación activa de la población en el diseño de las políticas de producción, distribución y consumo de alimentos."
            },
            {
              id: 3,
              line: "Iniciativas para el reconocimiento de los derechos de las mujeres en el uso y control de los recursos."
            },
            {
              id: 4,
              line: "Capacitaciones e iniciativas que promuevan el uso sostenible de los recursos productivos básicos –tierras, agua, bosques, ríos, mares, semillas y especies animales autóctonas – y el respeto a la biodiversidad."
            },
            {
              id: 5,
              line: "Estrategias para el uso de técnicas agropecuarias tradicionales respetuosas con el medio ambiente, con las tradiciones culturales y con las formas de trabajo y de relación con la naturaleza de los pueblos."
            },
            {
              id: 6,
              line: "Capacitaciones, creación de redes o de espacios para la conservación y el libre acceso al uso de semillas y especies animales criollas."
            },
            {
              id: 7,
              line: "Formación y fomento de consumo de alimentos inocuos, nutritivos y culturalmente apropiados."
            },
            {
              id: 8,
              line: "Construcción y fortalecimiento de espacios y redes locales, nacionales e internacionales defensoras de los derechos de productores y productoras y de consumidores y consumidoras."
            },
            {
              id: 9,
              line: "Propuestas de producción y consumo alimentario basadas en un sistema de producción agropecuaria diversificado, que respete la biodiversidad, maneje los recursos naturales de forma sostenibles y utilice tecnología apropiada, siempre respetando las tradiciones autóctonas, su diversidad cultural, sus formas de trabajo y su relación con la naturaleza."
            },
            {
              id: 10,
              line: "Promoción los mercados locales, basados en la producción local y el consumo ético como forma de auto sostenimiento, de conservación de culturas alimentarias propias o las demás que considere pertinentes y se ajusten a la línea con la naturaleza."
            }
          ]
    },
    {
        lineId: 1,
        lineName: "Línea 2. Proyectos productivos para Consejos Comunitarios y distintas expresiones organizativas de comunidades NARP.",
        lines: [
            {
              id: 1,
              line: "Mejoramiento u optimización de buenas prácticas productivas."
            },
            {
              id: 2,
              line: "Fortalecimiento de actividades agropecuarias relacionadas con la Vocación productiva de los territorios."
            },
            {
              id: 3,
              line: "Alternativas productivas basadas en Sistemas productivos tradicionales."
            },
            {
              id: 4,
              line: "Apoyar los procesos o acciones productivas, ambientales y culturales que permiten a las personas innovar y recrear procesos de economía asociativa, productividad, ecológicas – ambientales y cultural."
            },
            {
              id: 5,
              line: "Las demás que considere pertinentes y se ajusten a la línea programática.   Cuál?"
            }
          ]
    },
    {
        lineId: 2,
        lineName: "Línea 3. Fortalecimiento organizativo de Consejos Comunitarios y demás formas organizativas de comunidades NARP",
        lines: [
            {
              id: 1,
              line: "Diseño, ajuste o implementación de planes de autodesarrollo."
            },
            {
              id: 2,
              line: "Diseño, creación o ajuste de reglamentos internos para los consejos comunitarios."
            },
            {
              id: 3,
              line: "Iniciativas de recuperación de la memoria, la identidad cultural y/o saberes ancestrales."
            },
            {
              id: 4,
              line: "Diseño o ajuste de planes estratégicos, planes de acción relacionados con la estructura organizativa."
            },
            {
              id: 5,
              line: "Iniciativas de procesos relacionados con titulación colectiva."
            },
            {
              id: 6,
              line: "Fortalecimiento, formación o asesoría de procesos relacionados con gobernabilidad y gobernanza de la organización."
            },
            {
              id: 7,
              line: "Fortalecimiento, formación o asesoría de procesos relacionados con exigibilidad de derechos."
            },
            {
              id: 8,
              line: "Los demás que considere pertinentes y se ajusten a la línea programática.  Cuál?"
            }
          ]
    },
    {
        lineId: 3,
        lineName: "Línea 4. Plataformas digitales, Desarrollo tecnológico y Dotación.",
        lines: [
            {
              id: 1,
              line: "Dotación de herramientas TIC"
            },
            {
              id: 2,
              line: "Acceso a base de datos y sistemas de información"
            },
            {
              id: 3,
              line: "Diseño de páginas web"
            },
            {
              id: 4,
              line: "Adecuación de plataformas digitales"
            },
            {
              id: 5,
              line: "Creación de contenidos transmedia"
            },
            {
              id: 6,
              line: "Dotación para emisoras comunitarias"
            },
            {
              id: 7,
              line: "Paneles solares"
            },
            {
              id: 8,
              line: "Cableado y Router"
            },
            {
              id: 9,
              line: "Antenas de transmisión o recepción"
            },
            {
              id: 10,
              line: "Las demás que considere pertinentes y se ajusten a la línea programática. Cuál?"
            }
          ]
    },
    {
        lineId: 4,
        lineName: "Línea 5. Infraestructura Comunitaria.  Solo aplica para Consejos Comunitarios",
        lines: [
            {
              id: 1,
              line: "La infraestructura que se construye para las comunidades se refiere principalmente a estructuras básicas pequeñas, infraestructuras técnicas y sistemas construidos a nivel local que son importantes para subsistencia de la población que vive en dichas comunidades.  Estas son infraestructuras pequeñas de bajo coste que se construyen con el tiempo a través de iniciativas llevadas por las comunidades de acuerdo con las necesidades y aspiraciones de la población."
            }
          ]
    }
]
