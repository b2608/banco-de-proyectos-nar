import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostsAndExpensesComponent } from './costs-and-expenses.component';

describe('CostsAndExpensesComponent', () => {
  let component: CostsAndExpensesComponent;
  let fixture: ComponentFixture<CostsAndExpensesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostsAndExpensesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CostsAndExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
