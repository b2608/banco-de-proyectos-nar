import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { User, OrganizationCoverageMapper, OrganizationTypeMapper, OrganizationCityTypeMapper } from '@app/interfaces/user';
import { ProponentExperience } from '@app/interfaces/project';
import { AuthService } from '@app/services/auth.service';
import { ReduxState } from '@app/interfaces/redux-state';
import { ApiResponse } from '@app/interfaces/api';
import { ActionTypes } from '@app/interfaces/redux-state';
import { Lines } from './lines';
import { Sections } from './sections';

const emptyProponentExperience = {
  entity: '',
  contractValue: '',
  vigency: {
      start: '',
      end: ''
  },
  address: '',
  cellPhone: '',
  certifierName: ''
};

@Component({
  selector: 'app-new-detail',
  templateUrl: './new-detail.component.html',
  styleUrls: ['./new-detail.component.sass']
})
export class NewDetailComponent implements OnInit {
  user$!: Observable<User>;
  user!: User;
  loading: boolean = true;
  projectPresentation!: FormGroup;
  organizationType: any = undefined;
  organizationTypeMApper = OrganizationTypeMapper;
  organizationCityType: any = undefined;
  organizationCityTypeMapper = OrganizationCityTypeMapper;
  organizationCoverage: any = undefined;
  organizationCoverageMapper = OrganizationCoverageMapper;
  proponentExperience: ProponentExperience[] = [emptyProponentExperience];
  liquidityIndicator: number = 0;
  linesForm = Lines;
  lineSelectedForm: any;
  sectionsForm = Sections;
  sectionSelected: any = null;
  divisionSelected: any = null;
  activitySelected: any = null;

  constructor(
    private formBuilder: FormBuilder, 
    private store: Store<ReduxState>,
    private authService: AuthService
    ) { 
    this.user$ = store.select('user');
    console.log(this.sectionsForm);
      this.user$.subscribe(async(data) =>{
        this.user = data;
        this.organizationType = await this.organizationTypeMApper.find(element => element.key == this.user.organizationType)?.value;
        this.organizationCityType = await this.organizationCityTypeMapper.find(element => element.key == this.user.comunity)?.value;
        this.organizationCoverage = await this.organizationCoverageMapper.find(element => element.key == this.user.coverage)?.value;
        setTimeout(()=> {
          this.loading = false;
        }, 3000);
      });
  }

  ngOnInit(): void {
    this.projectPresentation= this.formBuilder.group({
      organizationName: [{value: "", disabled: true}, Validators.required],
      legalRepresentative: [{value: "", disabled: true}, Validators.required],
      legalRepresentativeDNI: [{value: "", disabled: true}, Validators.required],
      organizationType: [{value: "", disabled: true}, Validators.required],
      comunity: [{value: "", disabled: true}, Validators.required],
      organizationCoverage: [{value: "", disabled: true}, Validators.required],
      affectsTheProperty: [null, Validators.required],
      adjudicator: ["", Validators.required],
      realEstateRegistration: ["", Validators.required],
      realEstateLocation: ["", Validators.required],
      realEstateOwner: ["", Validators.required],
      realEstateFrom: ["", Validators.required],
      realEstateTo: ["", Validators.required],
      departmentalReferencePopulation: ["", Validators.required],
      municipalReferencePopulation: ["", Validators.required],
      targetPopulationBenefiting: ["", Validators.required],
      proponentExperience: [false, Validators.required],
      proponentExperiences1: this.formBuilder.group({
        entity: [""],
        contractValue: [""],
        vigency: this.formBuilder.group({
            start: [""],
            end: ''
        }),
        address: [""],
        cellPhone: [""],
        certifierName: [""],
      }),
      proponentExperiences2: this.formBuilder.group({
        entity: [""],
        contractValue: [""],
        vigency: this.formBuilder.group({
            start: [""],
            end: [""],
        }),
        address: [""],
        cellPhone: [""],
        certifierName: [""],
      }),
      liquidityIndex: this.formBuilder.group({
        assets: [ Validators.required],
        passives: [ Validators.required],
        liquidityIndicator: [{value: 0, disabled: true}, Validators.required]
    }),
    line: [null, Validators.required],
    subline: [null, Validators.required],
    sectionCIIU: [null, Validators.required],
    divisionCIIU: [null, Validators.required],
    activityCIIU: [null, Validators.required],
    activityCIIUCode: [null, Validators.required],
    });
    this.getUserInfo();
  }

  getUserInfo(){
    this.authService.me().subscribe(async(data: ApiResponse) => {
      await this.store.dispatch({type: ActionTypes.FILL_USER, payload: data});
      this.projectPresentation.setValue({
        organizationName: this.user.organizationName,
        legalRepresentative: this.user.legalRepresentative,
        legalRepresentativeDNI: this.user.legalRepresentativeDNI,
        organizationType: this.user.organizationType,
        comunity: this.user.comunity,
        organizationCoverage: this.user.coverage,
        affectsTheProperty: null,
        adjudicator: "",
        realEstateRegistration: "",
        realEstateLocation: "",
        realEstateOwner: "",
        realEstateFrom: "",
        realEstateTo: "",
        departmentalReferencePopulation: "",
        municipalReferencePopulation: "",
        targetPopulationBenefiting: "",
        proponentExperience: false,
        proponentExperiences1: {
          entity: "",
          contractValue: "",
          vigency: {
              start: "",
              end: ''
          },
          address: "",
          cellPhone: "",
          certifierName: "",
        },
        proponentExperiences2: {
          entity: "",
          contractValue: "",
          vigency: {
              start: "",
              end: "",
          },
          address: "",
          cellPhone: "",
          certifierName: "",
        },
        liquidityIndex: {
          assets: null,
          passives: null,
          liquidityIndicator: 0
      },
      line: null,
      subline: null,
      sectionCIIU: null,
      divisionCIIU: null,
      activityCIIU: null,
      activityCIIUCode: null,
      });
    });
  }

  newproponentExperience () {
    this.proponentExperience.push(emptyProponentExperience);
  }

  isNumber(valA: any, valB: any): boolean { 
    this.liquidityIndicator = parseInt(valA) / parseInt(valB);
    return !Number.isNaN(this.liquidityIndicator);
   }


  sendForm() {
    this.projectPresentation.setValue({
      organizationName: this.user.organizationName,
      legalRepresentative: this.user.legalRepresentative,
      legalRepresentativeDNI: this.user.legalRepresentativeDNI,
      organizationType: this.user.organizationType,
      comunity: this.user.comunity,
    });
  }
  async selectedLine(){
    console.log(this.projectPresentation.value.line);
    const currentLine = this.projectPresentation.value.line;
    const selected = await this.linesForm.find((line:any) => line.lineId === currentLine);
    console.log(this.linesForm)
    this.lineSelectedForm = selected;
  }

  selectSection() {
    this.sectionSelected = this.projectPresentation.value.sectionCIIU;
    this.divisionSelected = null;
    this.activitySelected = null;
  }

  selectDivision() {
    this.divisionSelected = this.projectPresentation.value.divisionCIIU;
    this.activitySelected = null;
  }

  selectactivity() {
    console.log(this.projectPresentation.value.sectionCIIU);
    this.activitySelected = this.projectPresentation.value.activityCIIU;
    /* this.projectPresentation.setValue({
      ...this.projectPresentation.value,
      activityCIIUCode: this.sectionsForm[this.sectionSelected].divisions[this.divisionSelected].activities[this.activitySelected].code,
    });
    console.log("AAAAAAAAAAAAAAAAAAAAAAAA")
    console.log(this.projectPresentation.value) */
  }

}
