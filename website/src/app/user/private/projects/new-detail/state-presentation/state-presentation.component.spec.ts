import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatePresentationComponent } from './state-presentation.component';

describe('StatePresentationComponent', () => {
  let component: StatePresentationComponent;
  let fixture: ComponentFixture<StatePresentationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatePresentationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatePresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
