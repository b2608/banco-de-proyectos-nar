import { Component, OnInit } from '@angular/core';
import { ProjectPresentation } from '@app/interfaces/project';

@Component({
  selector: 'app-project-presentation, [app-project-presentation]',
  templateUrl: './project-presentation.component.html',
  styleUrls: ['./project-presentation.component.sass']
})
export class ProjectPresentationComponent implements OnInit {

  
  constructor() { }

  ngOnInit(): void {
  }

}
