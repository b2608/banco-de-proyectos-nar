import { Component, OnInit } from '@angular/core';
import { ReduxState } from '@app/interfaces/redux-state';
import { Store } from '@ngrx/store';
import { AuthService } from '@app/services/auth.service';
import { Router } from "@angular/router";
import { ApiResponse } from '@app/interfaces/api';
import { ActionTypes } from '@app/interfaces/redux-state';

@Component({
  selector: 'app-user-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.sass']
})
export class ProjectsComponent implements OnInit {

  constructor(
    private store: Store<ReduxState>, 
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.getUserInfo();
  }

  getUserInfo(){
    this.authService.me().subscribe(async(data: ApiResponse) => {
      console.log(data)
      await this.store.dispatch({type: ActionTypes.FILL_USER, payload: data});
    });
  }

  goToNewProject(){
        this.router.navigate(['/user/projects/in-project']);

  }

}
