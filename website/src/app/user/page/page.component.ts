import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '@app/interfaces/user';
import { ReduxState } from '@app/interfaces/redux-state';
import { ActionTypes } from '@app/interfaces/redux-state';
import { AuthService } from '@app/services/auth.service';
import { ApiResponse } from '@app/interfaces/api';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.sass']
})
export class PageComponent implements OnInit {
  user$!: Observable<User>;
  user!: User;
  loginform!: FormGroup;
  submittedForm: boolean = false;

  constructor(
    private store: Store<ReduxState>, 
    private formBuilder: FormBuilder, 
    private router: Router,
    private authService: AuthService
    ) { 

    this.store.subscribe(state => {
      console.log(state);
      this.user$ = store.select('user');
      this.user$.subscribe(data => this.user = data);
    });
  }

  ngOnInit(): void {
    this.loginform = this.formBuilder.group({
      userLogin: ["", Validators.required],
      password: ["", Validators.required],
      saveLogin: [false],
    })
    console.log("FORM LOGIN");
    console.log(this.loginform);
    this.getUserInfo();
  }

  getUserInfo(){
    this.authService.me().subscribe(async(data: ApiResponse) => {
      console.log(data)
      await this.store.dispatch({type: ActionTypes.FILL_USER, payload: data});
    });
  }

  async updateUser(){
    console.log("FORM PROFILE");
    console.log(this.loginform);
    // this.user = {
    
    // }
    await this.store.dispatch({type: ActionTypes.FILL_USER, payload: this.user});
    console.log(this.store);
    this.router.navigate(['/auth'])
  }
}
