export interface User {
    organizationName: string,
    organizationDNI: string,
    legalRepresentative: string,
    legalRepresentativeDNI: string,
    organizationType: OrganizationType,
    coverage: OrganizationCoverage,
    state: string,
    city: string,
    township: string,
    phoneNumber: string,
    comunity: string,
    email: string,
    website: string,
    password: string,
    publicRegister: {
        registerNumber: string,
        year: number,
        accreditingEntity: string,
        entityThatAccreditsTheUpdate: string,
        yearOfAccreditation: number,
      }
};

export enum OrganizationType {
    A = 'Consejos Comunitarios con título colectivo',
    B = 'Consejos Comunitarios con título en trámite',
    C = 'Consejos Comunitarios con posesión',
    D = 'Organizaciones de Base y diferentes expresiones organizativas NARP',
    E = 'Organizaciones de Segundo Nivel',
    F = 'Otro'
}


export const OrganizationTypeMapper: {
    key: string;
    value: string;
  }[] = Object.entries(OrganizationType)
    .map(([key, value]) => {
        return({ key, value })
    });

export enum OrganizationCoverage {
    A = 'Nacional',
    B = 'Regional',
    C = 'Local'
}

export const OrganizationCoverageMapper: {
    key: string;
    value: string;
  }[] = Object.entries(OrganizationCoverage)
    .map(([key, value]) => {
        return({ key, value })
    });

export enum OrganizationCityType {
    A = 'Comunidad Negra',
    B = 'Población Afrocolombiana',
    C = 'Población Raizal',
    D = 'Población Palenquera'
}

export const OrganizationCityTypeMapper: {
    key: string;
    value: string;
  }[] = Object.entries(OrganizationCityType)
    .map(([key, value]) => {
        return({ key, value })
    });

export enum Components {
    A = 'Componente 1. Fortalecimiento por Departamento	',
    B = 'Componente 2. Convocatoria Abierta',
}


export const ComponentsMapper: {
    key: string;
    value: string;
  }[] = Object.entries(Components)
    .map(([key, value]) => {
        return({ key, value })
    });


    export enum Lines {
        A = 'Línea 1. Soberania y Autonomía Alimentaria',
        B = 'Línea 2. Proyectos productivos para Consejos Comunitarios y distintas expresiones organizativas de comunidades NARP.',
        C = 'Línea 3. Fortalecimiento organizativo de Consejos Comunitarios y demás formas organizativas de comunidades NARP',
        D = 'Línea 4. Plataformas digitales, Desarrollo tecnológico y Dotación.',
        E = 'Línea 5. Infraestructura Comunitaria.  Solo aplica para Consejos Comunitarios'
    }
    
    export const LinesMapper: {
        key: string;
        value: string;
      }[] = Object.entries(Lines)
        .map(([key, value]) => {
            return({ key, value })
        });

export interface UserLogin {
    email?: string,
    password?: string
};

export interface UserRegister {
    organizationName: string,
    organizationDNI: string,
    legalRepresentative: string,
    legalRepresentativeDNI: string,
    organizationType: OrganizationType,
    coverage: OrganizationCoverage,
    state: string,
    city: string,
    township: string,
    phoneNumber: string,
    email: string,
    website: string,
    publicRegister: {
        registerNumber: string,
        year: number,
        accreditingEntity: string,
        entityThatAccreditsTheUpdate: string,
        yearOfAccreditation: number,
      },
    password: string,
};
