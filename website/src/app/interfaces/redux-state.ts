import { User } from './user';

export interface ReduxState {
    user: any
}

export interface Action {
    type: string;
    payload?: any;
  }

export const ActionTypes = {
    FILL_USER: 'Fill user data',
    UPDATE_USER:'Update user data'
   };