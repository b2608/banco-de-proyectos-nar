import { OrganizationType, Components, Lines } from '@app/interfaces/user';

export interface ProjectPresentation {
    owner?: Owner,
    organization?: Organization
    projectGeoInformation?: ProjectGeo
}

export interface Owner {
    ownerNAme: string,
    ownerJobPosition:string,
    appointmentResolution: string,
    instance: string,
    age: number,
    address: string,
    phoneNumber: number,
    website: string
}

export interface Organization {
    organizationName:string,
    organiztionType: OrganizationType
    anotherOrganizationType: string
}



export interface ProjectGeo {
    state: string,
    town: string,
    township: string,
    municipality: string,
    commune: string
}





export interface ProjectPresentation{
    projectName: string,
    publicRegister: {
        registerNumber: string,
        year: string,
        accreditingEntity: string,
        entityThatAccreditsTheUpdate: string,
        yearOfAccreditation: string
    },
    component: {
        component: Components,
        state: string
    }
    line: Lines
}

export interface ProponentExperience {
    entity: string,
    contractValue: string,
    vigency: {
        start: string,
        end: string
    },
    address: string,
    cellPhone: string,
    certifierName: string
}