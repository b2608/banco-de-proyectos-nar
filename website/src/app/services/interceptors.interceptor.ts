import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class InterceptorsInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    console.log("En interceptor");
    let headers = new HttpHeaders({
      'content-type': 'application/json'
    });
    const hasAuthToken = localStorage.getItem('authToken');
    if(hasAuthToken && hasAuthToken.length){
      headers = headers.set('Authorization', hasAuthToken);
    }
    

    const clone = request.clone({
      headers
    });

    return next.handle(clone);
  }
}
