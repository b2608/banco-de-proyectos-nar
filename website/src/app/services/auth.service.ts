import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UserLogin, UserRegister } from '@app/interfaces/user';
import { ApiResponse } from '@app/interfaces/api';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  api: string = "https://hw8hxio0v6.execute-api.us-east-1.amazonaws.com/dev/auth";
  constructor(private http: HttpClient, private router: Router) { }

  register(user: UserRegister): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.api}/register`, user);
  }
  login(user: UserLogin): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(`${this.api}/login`, user);
  }
  me(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${this.api}/me`);
  }
}
