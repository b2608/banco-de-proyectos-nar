import { Application, NextFunction, Request, Response, Router } from "express";

function HealthCheck(app: Application) {
  const router: Router = Router();
  app.use("/integration-services", router);

  router.get('/',
    async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<Response> => {
      return res.status(200).json({ message: "OK"});
    }
  );
}

export default HealthCheck;
