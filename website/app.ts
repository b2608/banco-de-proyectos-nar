import 'reflect-metadata';
import { ngExpressEngine } from '@nguniversal/express-engine';
import { join } from 'path';
import { existsSync } from 'fs';
import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import * as passport from "passport";
import * as express from "express";
import * as morgan from "morgan";
import * as cors from "cors";
import * as fileUpload from "express-fileupload";
import healthCheck from "./src/api/controllers/HealthCheck";

const distFolder = join(process.cwd(), 'dist/lifexp/browser');
const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

const { STAGE } = process.env;
export class App {
  private app: express.Application;
  constructor() {
    this.app = express();
    
    this.settings();
    this.middlewares();
    this.routes();
    this.handlers();
  }

  private settings() {

    // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
    this.app.engine('html', ngExpressEngine({
        bootstrap: AppServerModule,
      }));
    this.app.set('view engine', 'html');
    this.app.set('views', distFolder);
  }

  private middlewares() {
    this.app.use(morgan("dev"));
    this.app.use(express.urlencoded({ extended: true, limit: '50mb' }));
    this.app.use(express.json({limit: '50mb'}));
    this.app.use(cors());
    this.app.use(passport.initialize());
    this.app.use(fileUpload({ useTempFiles : true, tempFileDir : '/tmp/', }));
    //passport.use(passportMiddleware);
  }

  private routes() {
    healthCheck(this.app);
    
    this.app.get('*.*', express.static(distFolder, {
        maxAge: '1y'
      }));
    
      // All regular routes use the Universal engine
      this.app.get('*', (req, res) => {
        res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
      });
  }

  private handlers() {
    //this.app.use(notFoundHandler);
  }

  public async listen(port: string) {
    await this.app.listen(port);
    switch (STAGE?.toUpperCase()) {
      case "DEV":
        console.log(`Server:\t\thttp://api-dev.lifexp.co:${port}`);
        break;
      case "PROD":
        console.log(`Server:\t\thttp://api.lifexp.co:${port}`);
        break;
      case "QA":
        console.log(`Server:\t\thttp://api-qa.lifexp.co:${port}`);
        break;
    }
  }
}
