/***************************************************************************************************
 * Load `$localize` onto the global scope - used if i18n tags appear in Angular templates.
 */
import '@angular/localize/init';
import 'zone.js/dist/zone-node';
// import * as mongoose from 'mongoose';

import { App } from './app';

async function run(): Promise<void> {
  const port: string = process.env['PORT'] || '4000';

  const server = new App();
  await server.listen(port);

  // mongoose
  //       .connect('mongodb+srv://dev_remote:GZCkcxgSWFCOrPgg@shipal.sou2e.mongodb.net/dev?retryWrites=true&w=majority')
  //       .then(async(db: any) => {
  //         console.log("Database:\tCONNECTED ON STAGE");
  //         console.log(db);
  //         await server.listen(port);
  //       })
  //       .catch((err: any) => {
  //         throw err;
  //       });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
