import * as cdk from '@aws-cdk/core';
import * as path from 'path';
import { Bucket } from '@aws-cdk/aws-s3';
import { HostedZone, CnameRecord, ARecord, AaaaRecord, RecordTarget } from '@aws-cdk/aws-route53';
import { Certificate, CertificateValidation } from '@aws-cdk/aws-certificatemanager';
import { CloudFrontTarget } from "@aws-cdk/aws-route53-targets";
import { CloudFrontWebDistribution , OriginProtocolPolicy, ViewerCertificate, SecurityPolicyProtocol, SSLMethod } from '@aws-cdk/aws-cloudfront';
import { BucketDeployment, Source } from '@aws-cdk/aws-s3-deployment';

// import { PolicyStatement } from '@aws-cdk/aws-iam';

// import * as sqs from '@aws-cdk/aws-sqs';

export class BancoDeProyectosNarStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const domainName = 'bancodeproyectosnarp2021.co';
    const subDomainName = 'stage';
    const zone = HostedZone.fromLookup(this, 'baseZone', {
      domainName: domainName
    });
    
    const certificate = new Certificate(this, 'Certificate', {
      domainName: `${domainName}`,
      validation: CertificateValidation.fromDns(zone), // Records must be added manually
    });

    // The code that defines your stack goes here
    const bucketPage = new Bucket(this, 'BucketPage', {
      websiteIndexDocument: 'index.html',
      publicReadAccess: true,
      bucketName: `${subDomainName}.${domainName}`,
      removalPolicy: cdk.RemovalPolicy.DESTROY
    });

    /* const bucketImages = new Bucket(this, 'bucketImages', {
      encryption: BucketEncryption.KMS,
      publicReadAccess: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY
    }); */

    // const bucketContainerPermissions = new PolicyStatement();
    // bucketContainerPermissions.addResources(bucketPage.bucketArn);
    // bucketContainerPermissions.addActions('s3:ListBucket');

    // const bucketPermissions = new PolicyStatement();
    // bucketPermissions.addResources(`${bucketPage.bucketArn}/*`);
    // bucketPermissions.addActions('s3:GetObjects'); 

    
    const cloudFront = new CloudFrontWebDistribution(this, 'CfWebDist', {
      viewerCertificate: {
        aliases: [`${subDomainName}.${domainName}`],
        props: {
          acmCertificateArn: certificate.certificateArn,
          sslSupportMethod: "sni-only",
        },
      },
      originConfigs:[
        {
          customOriginSource: {
            domainName: bucketPage.bucketWebsiteDomainName,
            originProtocolPolicy: OriginProtocolPolicy.HTTP_ONLY,
        },
          behaviors: [
            {
              isDefaultBehavior: true
            }
          ]
        }
      ],
    });

    new BucketDeployment(this, 'BucketDeployment', {
      sources: [Source.asset(path.join(__dirname, "..", "website", "dist"))],
      destinationBucket: bucketPage,
      distribution: cloudFront,
      distributionPaths: ["/*"]
    });

    const cName = new CnameRecord(this, 'test.baseZone', {
      zone: zone,
      recordName: subDomainName,
      domainName: bucketPage.bucketWebsiteDomainName
    });

    new cdk.CfnOutput(this, 'cNamePrint', {
      value: cName.domainName,
      exportName: 'cNamePrint',
    });

    new cdk.CfnOutput(this, 'BucketPageExport', {
      value: bucketPage.bucketWebsiteUrl,
      exportName: 'BucketPageName',
    });

    new cdk.CfnOutput(this, 'SimplePageUrl', {
      value: cloudFront.distributionDomainName,
      exportName: 'SimplePageUrl',
    });

    new cdk.CfnOutput(this, 'BaseZonePrint', {
      value: zone.hostedZoneId,
      exportName: 'BaseZonePrint',
    });


    // const newUser = new lambda.NodejsFunction(this, 'newUsers', {
    //   runtime: Runtime.NODEJS_14_X,
    //   entry: path.join(__dirname, '..', 'api', 'src', 'components', 'auth', 'main.js' ),
    //   handler: 'handler',
    //   environment: {
    //     MONGO_DB_DEV: "mongodb+srv://bleeper-group-develop:26r6YwT14dcC1tfd@bleeperbuy.vkcyt.mongodb.net/develop?retryWrites=true&w=majority",
    //     IMAGES_BUCKET_NAME: bucketImages.bucketName,
    //     TOKEN: "BleeperGroupDev*2021*!Rockyeah¡"
    //   },
    // });



    // const httpApi = new HttpApi(this, 'HttpApi',{
    //   corsPreflight:{
    //     allowOrigins: ['*'],
    //     allowMethods: [
    //       CorsHttpMethod.GET,
    //       CorsHttpMethod.HEAD,
    //       CorsHttpMethod.OPTIONS,
    //       CorsHttpMethod.POST,
    //       CorsHttpMethod.PUT,
    //       CorsHttpMethod.DELETE,
    //     ],
    //   },
    //   apiName: 'users',
    //   createDefaultStage: true
    // });

    // httpApi.addRoutes({
    //   path: '/auth',
    //   methods: [HttpMethod.POST, HttpMethod.GET, HttpMethod.PUT, HttpMethod.DELETE, HttpMethod.OPTIONS, HttpMethod.HEAD],
    //   integration:  new LambdaProxyIntegration({
    //     handler: newUser
    //   }),
    // });


   
  }
}
